#include "aufgabe9.h"

void init_measuring(int type) {
	ADC_VBATCmd(DISABLE);
	ADC_TempSensorVrefintCmd(DISABLE);

	// ADC1 und GPIO Taktquelle einschalten
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	if (type == TYPE_EXTERN_VOLTAGE || type == TYPE_POWER_CONSUMPTION) {
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_InitStructure.GPIO_Pin =
				(type == TYPE_EXTERN_VOLTAGE) ? GPIO_Pin_0 : GPIO_Pin_1;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
	}

	//ADC1 - ADC_CommonInitTypeDef
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div8;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	//ADC1 - ADC_InitTypeDef
	ADC_InitTypeDef ADC_InitStructure;
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_StructInit(&ADC_InitStructure);

	// ADC Konfiguration je nach Messtyp
	if (type == TYPE_EXTERN_VOLTAGE) {
		ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 1,
				ADC_SampleTime_3Cycles);
	} else if (type == TYPE_POWER_CONSUMPTION) {
		ADC_RegularChannelConfig(ADC1, ADC_Channel_9, 1,
				ADC_SampleTime_3Cycles);
	} else if (type == TYPE_BATTERY_VOLTAGE) {
		ADC_RegularChannelConfig(ADC1, ADC_Channel_Vbat, 1,
				ADC_SampleTime_3Cycles);
	} else if (type == TYPE_TEMPERATURE) {
		ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1,
				ADC_SampleTime_480Cycles);
	}

	// ADC1 aktivieren
	ADC_Cmd(ADC1, ENABLE);
	if (type == TYPE_BATTERY_VOLTAGE) {
		ADC_VBATCmd(ENABLE);
	} else if (type == TYPE_TEMPERATURE) {
		ADC_TempSensorVrefintCmd(ENABLE);
	}

}

float get_measure_value(int type) {
	// Starte Konvertierung
	ADC_SoftwareStartConv(ADC1);
	// Warte bis der Wert verfügbar ist
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
		;
	float measured_value = (float) ADC_GetConversionValue(ADC1);
	float voltage = (measured_value / 4095.0f) * 3.3f;
	// Berechne je nach Typ den entsprechenden Wert
	if (type == TYPE_EXTERN_VOLTAGE)
		return voltage;
	else if (type == TYPE_POWER_CONSUMPTION)
		return voltage / (0.048f);
	else if (type == TYPE_BATTERY_VOLTAGE)
		return 2.0f * voltage + 0.445f;
	else if (type == TYPE_TEMPERATURE)
		return ((voltage - 0.76f) / 0.0025f) + 25.0f;
	return 0;
}

void aufgabe9_1_1() {
	// LEDs initialisieren
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_leds();

	// USART initialisieren
	init_usart_2();

	// PWM für Signalausgabe initialisieren
	init_pwm();

	// Spannungsmessung initialisieren
	init_measuring(TYPE_EXTERN_VOLTAGE);

	while (1) {
		float voltage = get_measure_value(TYPE_EXTERN_VOLTAGE);
		LED_OFF;
		if (voltage <= 0.9f) {
			RT_LED_ON;
			init_pwm_signal_timer(NOTE_C);
		} else if (voltage < 1.4f) {
			GB_LED_ON;
			disable_pwm_timer();
		} else {
			GR_LED_ON;
			disable_pwm_timer();
		}

		sprintf(usart2_tx_buffer, "Voltage: %.1f V\r\n", voltage);
		usart_2_print(usart2_tx_buffer);

		wait_sec(1);
	}
}

void aufgabe9_1_2() {
	init_usart_2();
	while (1) {
		init_measuring(TYPE_POWER_CONSUMPTION);
		float value = get_measure_value(TYPE_POWER_CONSUMPTION);
		sprintf(usart2_tx_buffer, "Power Consumption: %.2f mA\r\n", value);
		usart_2_print(usart2_tx_buffer);

		init_measuring(TYPE_BATTERY_VOLTAGE);
		value = get_measure_value(TYPE_BATTERY_VOLTAGE);
		sprintf(usart2_tx_buffer, "Battery Voltage: %.1f V\r\n", value);
		usart_2_print(usart2_tx_buffer);

		init_measuring(TYPE_TEMPERATURE);
		value = get_measure_value(TYPE_TEMPERATURE);
		sprintf(usart2_tx_buffer, "Temperature: %.1f °C\r\n", value);
		usart_2_print(usart2_tx_buffer);

		wait_sec(1);
	}
}
