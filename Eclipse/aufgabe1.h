#ifndef __AUFGABE1_H__
#define __AUFGABE1_H__

//=========================================================================
// cmsis_lib
//=========================================================================
#include "stm32f4xx.h"
//#include "misc.h"
//#include "stm32f4xx_adc.h"
//#include "stm32f4xx_can.h"
//#include "stm32f4xx_crc.h"
//#include "stm32f4xx_cryp_aes.h"
//#include "stm32f4xx_cryp_des.h"
//#include "stm32f4xx_cryp_tdes.h"
//#include "stm32f4xx_cryp.h"
//#include "stm32f4xx_dac.h"
//#include "stm32f4xx_dbgmcu.h"
//#include "stm32f4xx_dcmi.h"
//#include "stm32f4xx_dma.h"
//#include "stm32f4xx_exti.h"
//#include "stm32f4xx_flash.h"
//#include "stm32f4xx_fsmc.h"
#include "stm32f4xx_gpio.h"
//#include "stm32f4xx_hash_md5.h"
//#include "stm32f4xx_hash_sha1.h"
//#include "stm32f4xx_hash.h"
//#include "stm32f4xx_i2c.h"
#include "stm32f4xx_iwdg.h"
//#include "stm32f4xx_pwr.h"
#include "stm32f4xx_rcc.h"
//#include "stm32f4xx_rng.h"
//#include "stm32f4xx_rtc.h"
//#include "stm32f4xx_sdio.h"
//#include "stm32f4xx_spi.h"
//#include "stm32f4xx_syscfg.h"
//#include "stm32f4xx_tim.h"
//#include "stm32f4xx_usart.h"
#include "stm32f4xx_wwdg.h"

//=========================================================================
// board_lib
//=========================================================================

//=========================================================================
// standart_lib
//=========================================================================
#include "spi.h"
//#include "stdio.h"
//#include "string.h"

//=========================================================================
// CooCox CoOS
//=========================================================================
//#include "CoOS.h"

//=========================================================================
// Eigene Funktionen, Macros und Variablen
//=========================================================================
//========== Macros

// 4

#define RT_LED_ON (GPIO_ResetBits(GPIOB, GPIO_Pin_8))
#define RT_LED_OFF (GPIO_SetBits(GPIOB, GPIO_Pin_8))
#define RT_LED_TOGGLE (GPIO_ToggleBits(GPIOB, GPIO_Pin_8))
#define GB_LED_ON (GPIO_ResetBits(GPIOB, GPIO_Pin_14))
#define GB_LED_OFF (GPIO_SetBits(GPIOB, GPIO_Pin_14))
#define GB_LED_TOGGLE (GPIO_ToggleBits(GPIOB, GPIO_Pin_14))
#define GR_LED_ON (GPIO_ResetBits(GPIOB, GPIO_Pin_15))
#define GR_LED_OFF (GPIO_SetBits(GPIOB, GPIO_Pin_15))
#define GR_LED_TOGGLE (GPIO_ToggleBits(GPIOB, GPIO_Pin_15))

// 5

#define AMPEL_RT {RT_LED_ON;GB_LED_OFF;GR_LED_OFF;}
#define AMPEL_RT_GB {RT_LED_ON;GB_LED_ON;GR_LED_OFF;}
#define AMPEL_GR  {RT_LED_OFF;GB_LED_OFF;GR_LED_ON;}
#define AMPEL_GB {RT_LED_OFF;GB_LED_ON;GR_LED_OFF;}

//========== Funktionen
void wait_sec(unsigned long s);
void init_leds(void);
void init_taste_1(void);
void init_taste_2(void);
uint8_t read_taste_1(void);
uint8_t read_taste_2(void);
void aufgabe1_1_4(void);
void aufgabe1_1_5(void);
void aufgabe1_1_6(void);
void aufgabe1_2_3(void);
void aufgabe1_2_4(void);

#endif
