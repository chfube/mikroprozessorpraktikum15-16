#include "aufgabe4.h"

//========== Macros

//========== Variablen
char usart2_rx_buffer[50];
char usart2_tx_buffer[50];

//========== Funktionen

void init_usart_2_tx() {
	// Taktsystem für die USART2 freigeben
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	// GPIO Port A Taktsystem freigeben
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// USART2 TX an PA2 mit Alternativfunktion Konfigurieren
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// USART2 TX mit PA2 verbinden
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);

	// Datenprotokoll der USART einstellen
	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 921600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl =
			USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure);

	// USART2 freigeben
	USART_Cmd(USART2, ENABLE); // enable USART2
}

void init_usart_2() {
	// Taktsystem für die USART2 freigeben
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	// GPIO Port A Taktsystem freigeben
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// USART2 TX an PA2 mit Alternativfunktion Konfigurieren
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// USART2 TX mit PA2 verbinden
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	// Datenprotokoll der USART einstellen
	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 921600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl =
			USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_Init(USART2, &USART_InitStructure);

	// USART2 freigeben
	USART_Cmd(USART2, ENABLE); // enable USART2
}

void usart_2_print(char * zeichenkette) {
	int i = 0;
	for (i = 0; i < strlen(zeichenkette); i++) {
		// Sende Zeichen
		USART_SendData(USART2, zeichenkette[i]);
		// Warte bis Übertragung abgeschlossen
		while (!USART_GetFlagStatus(USART2, USART_FLAG_TC)) {
		}
	}
}

char usart_2_read() {
	while (!USART_GetFlagStatus(USART2, USART_IT_RXNE)) {
	}
	// Zeichen lesen und auf ein char casten
	return (char) USART_ReceiveData(USART2);
}

void aufgabe4_1_1() {
	// Initialisiere usart zum senden
	init_usart_2_tx();
	// Sende Zeichen 0,1,...,9
	while (1) {
		char i;
		for (i = '0'; i <= '9'; i++) {
			USART_SendData(USART2, i);
			// warte bis das Senden abgeschlossen ist
			while (!USART_GetFlagStatus(USART2, USART_FLAG_TC)) {
			}
			wait_sec(1);
		}
	}
}

void aufgabe4_1_2() {
	// Initialisiere usart zum senden
	init_usart_2_tx();
	unsigned int i = 0;
	while (1) {
		sprintf(usart2_tx_buffer, "Durchlauf No %u\r\n", i++);
		// Sendet usart2_tx_buffer
		usart_2_print(usart2_tx_buffer);
	}
}

void aufgabe4_1_3() {
	// USART initialisieren
	init_usart_2();

	// LEDs initialisieren
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_leds();

	while (1) {
		char c = usart_2_read();
		// Wenn 1 eingegeben wurde toggle die rote LED und sende eine Nachricht über usart
		if (c == '1') {
			RT_LED_TOGGLE;
			strcpy(usart2_tx_buffer, "1 - rote LED \r\n");
			usart_2_print(usart2_tx_buffer);
		// Wenn 2 eingegeben wurde toggle die gelbe LED und sende eine Nachricht über usart
		} else if (c == '2') {
			GB_LED_TOGGLE;
			strcpy(usart2_tx_buffer, "2 - gelbe LED \r\n");
			usart_2_print(usart2_tx_buffer);
		// Wenn 3 eingegeben wurde toggle die grüne LED und sende eine Nachricht über usart
		} else if (c == '3') {
			GR_LED_TOGGLE;
			strcpy(usart2_tx_buffer, "3 - grüne LED \r\n");
			usart_2_print(usart2_tx_buffer);
		}
	}
}

void aufgabe4_1_4() {
	// USART initialisieren
	init_usart_2();
	// Die Größe des Eingabepuffers
	int read_buffer_size = sizeof(usart2_rx_buffer);
	// Die aktuelle Länge der aktuellen Eingabe
	int length = 0;
	while (1) {
		// Lese ein Zeichen
		char c = usart_2_read();
		// Wenn mit der Eingabetaste bestätigt wurde oder der Puffer voll ist
		if (c == '\r' || length == read_buffer_size - 1) {
			// Terminire die Zeichenkette
			usart2_rx_buffer[length] = '\0';
			// Sende die Eingabe zurück
			usart_2_print(usart2_rx_buffer);
			// Wenn der Puffer voll war sende einen Zeilenumbruch
			if (length == read_buffer_size - 1) {
				usart_2_print("\r\n");
			}
			// Sende anschließend die Länge der Eingabe
			sprintf(usart2_tx_buffer, ": %d\r\n", length);
			usart_2_print(usart2_tx_buffer);
			length = 0;
		} else if (c == 0x08) { // Wenn Backspace
			// Gehe ein Zeichen zurück
			if (length > 0)
				length--;
		} else if (c != '\n') {
			// Merken des Zeichens und inkrementiere die Länge
			usart2_rx_buffer[length++] = c;
		}
	}
}