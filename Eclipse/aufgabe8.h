#ifndef __AUFGABEH__
#define __AUFGABE8_H__

#include <math.h>

//=========================================================================
// cmsis_lib
//=========================================================================
#include "stm32f4xx.h"
#include "misc.h"
//#include "stm32f4xx_adc.h"
//#include "stm32f4xx_can.h"
//#include "stm32f4xx_crc.h"
//#include "stm32f4xx_cryp_aes.h"
//#include "stm32f4xx_cryp_des.h"
//#include "stm32f4xx_cryp_tdes.h"
//#include "stm32f4xx_cryp.h"
//#include "stm32f4xx_dac.h"
//#include "stm32f4xx_dbgmcu.h"
//#include "stm32f4xx_dcmi.h"
//#include "stm32f4xx_dma.h"
//#include "stm32f4xx_exti.h"
//#include "stm32f4xx_flash.h"
//#include "stm32f4xx_fsmc.h"
//#include "stm32f4xx_gpio.h"
//#include "stm32f4xx_hash_md5.h"
//#include "stm32f4xx_hash_sha1.h"
//#include "stm32f4xx_hash.h"
//#include "stm32f4xx_i2c.h"
//#include "stm32f4xx_iwdg.h"
//#include "stm32f4xx_pwr.h"
//#include "stm32f4xx_rcc.h"
#include "stm32f4xx_rng.h"
//#include "stm32f4xx_rtc.h"
//#include "stm32f4xx_sdio.h"
//#include "stm32f4xx_spi.h"
//#include "stm32f4xx_syscfg.h"
#include "stm32f4xx_tim.h"
//#include "stm32f4xx_usart.h"
//#include "stm32f4xx_wwdg.h"

#include "aufgabe4.h"

//=========================================================================
// board_lib
//=========================================================================

//=========================================================================
// standart_lib
//=========================================================================
#include "spi.h"
//#include "MPP/BOARD/rtc.h"
//#include "stdio.h"
//#include "string.h"

//=========================================================================
// CooCox CoOS
//=========================================================================
//#include "CoOS.h"

//=========================================================================
// Eigene Funktionen, Macros und Variablen
//=========================================================================
//========== Variablen

unsigned long timer_counter;
int aufgabe_8_aktiv;

//========== Macros

#define NOTE_C 261.626
#define NOTE_CIS 277.183
#define NOTE_D 293.665
#define NOTE_DIS 311.127
#define NOTE_E 329.628
#define NOTE_F 349.228
#define NOTE_FIS  369.994
#define NOTE_G 391.995
#define NOTE_GIS 415.305
#define NOTE_A 440.0
#define NOTE_AIS 466.164
#define NOTE_H  493.883
#define NOTE_c 523.251
#define NOTE_cis 554.365
#define NOTE_d 587.330
#define NOTE_dis 622.254
#define NOTE_e 659.255
#define NOTE_f 698.456
#define NOTE_fis 739.989
#define NOTE_g 783.991

#define NOTE_RING 0.0
#define NOTE_PAUSE 1.0

//========== Funktionen

void init_pwm();
void init_pwm_signal_timer(float frequency);
void disable_pwm_timer();
void init_tim7_interrupt(unsigned long hz);
uint32_t random(void);

void aufgabe8_1_1();
void aufgabe8_1_2();
void aufgabe8_1_3();
void aufgabe8_1_4();
void aufgabe8_2_1();
void aufgabe8_2_2();
void aufgabe8_2_3();

#endif
