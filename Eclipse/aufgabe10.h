#ifndef __AUFGABEH__
#define __AUFGABE9_H__

#include <math.h>

//=========================================================================
// cmsis_lib
//=========================================================================
#include "stm32f4xx.h"
#include "misc.h"
//#include "stm32f4xx_adc.h"
//#include "stm32f4xx_can.h"
//#include "stm32f4xx_crc.h"
//#include "stm32f4xx_cryp_aes.h"
//#include "stm32f4xx_cryp_des.h"
//#include "stm32f4xx_cryp_tdes.h"
//#include "stm32f4xx_cryp.h"
#include "stm32f4xx_dac.h"
//#include "stm32f4xx_dbgmcu.h"
//#include "stm32f4xx_dcmi.h"
//#include "stm32f4xx_dma.h"
//#include "stm32f4xx_exti.h"
//#include "stm32f4xx_flash.h"
//#include "stm32f4xx_fsmc.h"
#include "stm32f4xx_gpio.h"
//#include "stm32f4xx_hash_md5.h"
//#include "stm32f4xx_hash_sha1.h"
//#include "stm32f4xx_hash.h"
//#include "stm32f4xx_i2c.h"
//#include "stm32f4xx_iwdg.h"
//#include "stm32f4xx_pwr.h"
#include "stm32f4xx_rcc.h"
//#include "stm32f4xx_rng.h"
//#include "stm32f4xx_rtc.h"
//#include "stm32f4xx_sdio.h"
//#include "stm32f4xx_spi.h"
//#include "stm32f4xx_syscfg.h"
//#include "stm32f4xx_tim.h"
//#include "stm32f4xx_usart.h"
//#include "stm32f4xx_wwdg.h"


//=========================================================================
// board_lib
//=========================================================================

//=========================================================================
// standart_lib
//=========================================================================
#include "spi.h"
//#include "MPP/BOARD/rtc.h"
//#include "stdio.h"
//#include "string.h"

#include "aufgabe8.h"
#include "aufgabe9.h"

//=========================================================================
// CooCox CoOS
//=========================================================================
//#include "CoOS.h"

//=========================================================================
// Eigene Funktionen, Macros und Variablen
//=========================================================================
//========== Variablen
int aufgabe_10_aktiv;

float sinus_voltage[100];
int sinus_x_pos;

float average_values[100];
int average_value_index;
int average_window_width;

//========== Macros


//========== Funktionen

void init_sinus_output();
unsigned int get_dau_value(float voltage, unsigned int dau_value_bit_range);
void set_output_voltage(float voltage, unsigned int dau_value_bit_range);

void aufgabe10_1_1();
void aufgabe10_1_2();

#endif
