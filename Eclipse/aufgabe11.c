#include "aufgabe11.h"

void print_usart2_dma(char* buffer) {
	// DMA Stream Register zurücksetzen
	DMA_DeInit(DMA1_Stream6);

	// Struct für die DMA Register anlegen
	DMA_InitTypeDef DMA_InitStructure;

	// DMA Register im Struct setzen
	DMA_InitStructure.DMA_Channel = DMA_Channel_4;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) buffer;
	DMA_InitStructure.DMA_BufferSize = (uint16_t) strlen(buffer);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART2->DR;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;

	// Im Struct gesetzte Werte in die DMA Register schreiben
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	// DMA Interface für die USART freigeben im Transmitmode freigeben
	USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);

	// DMA Stream Interrupt für Transfer Complete freigeben
	DMA_ITConfig(DMA1_Stream6, DMA_IT_TC, ENABLE);

	// DMA Transmitmode Stream freigeben
	DMA_Cmd(DMA1_Stream6, ENABLE);

	// jetzt erfolgt die Übertragung der Daten durch die DMA
	// wenn der Transfer fertig ist, wird die ISR aufgerufen
}

void aufgabe11_1_1() {
	// Priority Group konfigurieren
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	// NVIC Register Struct anlegen
	NVIC_InitTypeDef NVIC_InitStructure;

	// DMA2 Stream6 Interrupt konfigurieren
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream6_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	// Clocksystem für die DMA zuschalten
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);

	aufgabe_11_aktiv = 1;
	// USART initialisieren
	init_usart_2();

	// RXNE interrupt erlauben
	// Erlaubt Interrupt wenn Zeichen Empfangen
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	// USART2 Interrupt global erlauben
	NVIC_EnableIRQ(USART2_IRQn);

	while (1) {

	}
}

void start_dma_sinus_output(unsigned long hz) {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_DMA1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC | RCC_APB1Periph_TIM7, ENABLE);

	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_TimeBaseStructure.TIM_Prescaler = 8400 - 1;
	TIM_TimeBaseStructure.TIM_Period = (10000 / hz) - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);

	TIM_SelectOutputTrigger(TIM7, TIM_TRGOSource_Update);
	TIM_Cmd(TIM7, ENABLE);

	DMA_DeInit(DMA1_Stream5);
	DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_Channel = DMA_Channel_7;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) sinus_dau;
	DMA_InitStructure.DMA_BufferSize = (uint16_t)(
			sizeof(sinus_dau) / sizeof(sinus_dau[0]));
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &DAC->DHR12R1;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream5, &DMA_InitStructure);

	DMA1_Stream5->CR |= DMA_SxCR_PSIZE_0;

	DMA_Cmd(DMA1_Stream5, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	DAC_InitTypeDef DAC_InitStructure;
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_T7_TRGO;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;
	DAC_Init(DAC_Channel_1, &DAC_InitStructure);

	DAC_DMACmd(DAC_Channel_1, ENABLE);
	DAC_Cmd(DAC_Channel_1, ENABLE);
}

void convert_sinus_to_dau() {
	int i = 0;
	for (i = 0; i < 100; i++) {
		sinus_dau[i] = get_dau_value(sinus_voltage[i], 12);
	}
}

void aufgabe11_2_1() {
	init_sinus_output();
	convert_sinus_to_dau();
	start_dma_sinus_output(sizeof(sinus_dau) / sizeof(sinus_dau[0]) * 50);
	while (1) {
	}
}
