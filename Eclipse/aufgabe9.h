#ifndef __AUFGABEH__
#define __AUFGABE9_H__

#include <math.h>

//=========================================================================
// cmsis_lib
//=========================================================================
#include "stm32f4xx.h"
#include "misc.h"
#include "stm32f4xx_adc.h"
//#include "stm32f4xx_can.h"
//#include "stm32f4xx_crc.h"
//#include "stm32f4xx_cryp_aes.h"
//#include "stm32f4xx_cryp_des.h"
//#include "stm32f4xx_cryp_tdes.h"
//#include "stm32f4xx_cryp.h"
//#include "stm32f4xx_dac.h"
//#include "stm32f4xx_dbgmcu.h"
//#include "stm32f4xx_dcmi.h"
//#include "stm32f4xx_dma.h"
//#include "stm32f4xx_exti.h"
//#include "stm32f4xx_flash.h"
//#include "stm32f4xx_fsmc.h"
//#include "stm32f4xx_gpio.h"
//#include "stm32f4xx_hash_md5.h"
//#include "stm32f4xx_hash_sha1.h"
//#include "stm32f4xx_hash.h"
//#include "stm32f4xx_i2c.h"
//#include "stm32f4xx_iwdg.h"
//#include "stm32f4xx_pwr.h"
//#include "stm32f4xx_rcc.h"
//#include "stm32f4xx_rng.h"
//#include "stm32f4xx_rtc.h"
//#include "stm32f4xx_sdio.h"
//#include "stm32f4xx_spi.h"
//#include "stm32f4xx_syscfg.h"
#include "stm32f4xx_tim.h"
//#include "stm32f4xx_usart.h"
//#include "stm32f4xx_wwdg.h"

#include "aufgabe4.h"
#include "aufgabe8.h"

//=========================================================================
// board_lib
//=========================================================================

//=========================================================================
// standart_lib
//=========================================================================
#include "spi.h"
//#include "MPP/BOARD/rtc.h"
//#include "stdio.h"
//#include "string.h"

//=========================================================================
// CooCox CoOS
//=========================================================================
//#include "CoOS.h"

//=========================================================================
// Eigene Funktionen, Macros und Variablen
//=========================================================================
//========== Variablen


//========== Macros
#define TYPE_EXTERN_VOLTAGE 1
#define TYPE_POWER_CONSUMPTION 2
#define TYPE_BATTERY_VOLTAGE 3
#define TYPE_TEMPERATURE 4

#define LED_OFF {RT_LED_OFF;GB_LED_OFF;GR_LED_OFF;}

//========== Funktionen

void init_measuring(int type);
float get_measure_value(int type);

void aufgabe9_1_1();
void aufgabe9_1_2();

#endif
