#include "aufgabe6.h"

const const char* WEEKDAYS[] = { "Montag", "Dienstag", "Mittwoch", "Donnerstag",
		"Freitag", "Samstag", "Sonntag" };

void aufgabe6_1_1() {
	// Real Time Clock initialisieren
	start_RTC();

	// USART für schreiben initialisieren
	init_usart_2_tx();

	// Anlegen der Structs für Datum und Zeit
	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	char buffer[50];

	while (1) {
		// Lesen des aktuellen Datums/Zeit
		RTC_GetDate(RTC_Format_BIN, &date);
		RTC_GetTime(RTC_Format_BIN, &time);
		sprintf(buffer,
				"Heute ist der %02i.%02i.%02i. Es ist %02i:%02i:%02i Uhr.\r\n",
				date.RTC_Date, date.RTC_Month, date.RTC_Year, time.RTC_Hours,
				time.RTC_Minutes, time.RTC_Seconds);
		usart_2_print(buffer);

		wait_sec(1);
	}
}

void aufgabe6_1_2() {
	// Real Time Clock initialisieren
	start_RTC();

	// USART initialisieren
	init_usart_2();

	// Interrupt für USART aktivieren
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	NVIC_EnableIRQ(USART2_IRQn);

	aufgabe_6_aktiv = 1;

	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	char buffer[50];

	while (1) {
		// Ausgabe nur, wenn nicht gerade die Zeit gesetzt wird
		if (!setting_time) {
			RTC_GetDate(RTC_Format_BIN, &date);
			RTC_GetTime(RTC_Format_BIN, &time);
			sprintf(buffer,
					"Heute ist %s der %02i.%02i.%02i. Es ist %02i:%02i:%02i Uhr.\r\n",
					WEEKDAYS[date.RTC_WeekDay - 1], date.RTC_Date,
					date.RTC_Month, date.RTC_Year, time.RTC_Hours,
					time.RTC_Minutes, time.RTC_Seconds);
			usart_2_print(buffer);
		}

		wait_sec(1);
	}
}

void set_monday_alarm() {
	// Jeden Montag um 00:30:00 soll ein Alarm A ausgelöst werden

	RTC_AlarmTypeDef RTC_Alarm_Struct;

	// Alarmzeit setzen..
	RTC_Alarm_Struct.RTC_AlarmTime.RTC_H12 = RTC_H12_AM;
	RTC_Alarm_Struct.RTC_AlarmTime.RTC_Hours = 0x00;
	RTC_Alarm_Struct.RTC_AlarmTime.RTC_Minutes = 0x30;
	RTC_Alarm_Struct.RTC_AlarmTime.RTC_Seconds = 0x00;

	// Alarmmaske setzen
	RTC_Alarm_Struct.RTC_AlarmMask = RTC_AlarmMask_None;

	RTC_Alarm_Struct.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_WeekDay;

	RTC_Alarm_Struct.RTC_AlarmDateWeekDay = RTC_Weekday_Monday;

	// Konfiguration von Alarm A
	RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_Alarm_Struct);
}

void set_frequent_alarm() {
	// Anlegen der Structs für aktuelle Daten
	RTC_AlarmTypeDef RTC_Alarm_Aktuell; //  Alarm

	RTC_Alarm_Aktuell.RTC_AlarmTime.RTC_H12 = RTC_H12_AM;
	RTC_Alarm_Aktuell.RTC_AlarmTime.RTC_Hours = 0x01;
	RTC_Alarm_Aktuell.RTC_AlarmTime.RTC_Minutes = 0x00;
	RTC_Alarm_Aktuell.RTC_AlarmTime.RTC_Seconds = 0x30;

	// Alarmmaske setzen kann auch verodert werden
	RTC_Alarm_Aktuell.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay // Wochentag oder Tag ausgeblendet
	| RTC_AlarmMask_Hours // Stunde ausgeblendet
			| RTC_AlarmMask_Minutes; // Minute ausgeblendet

	// Alarm für den Tag oder Wochentag auswählen
	RTC_Alarm_Aktuell.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date; // Tag (1-31)
	// Alarm Tag oder Wochentag setzen
	RTC_Alarm_Aktuell.RTC_AlarmDateWeekDay = 0x01; // Tag 0x01...0x31

	RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_Alarm_Aktuell);
}

void init_alarm_interrupt() {
	// RTC Alarm A Interruptkonfiguration

	// Anlegen der benötigten Structs
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	// EXTI-Line Konfiguration
	EXTI_ClearITPendingBit(EXTI_Line17);
	EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	// NIVC Konfiguration
	NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Konfigurieren des Alarm A
	RTC_ITConfig(RTC_IT_ALRA, ENABLE);

	// RTC Alarm A freigeben
	RTC_AlarmCmd(RTC_Alarm_A, ENABLE);

	// Alarmflag löschen
	RTC_ClearFlag(RTC_FLAG_ALRAF);
}

void aufgabe6_2_1(int part) {
	// Real Time Clock initialisieren
	start_RTC();

	// USART initialisieren
	init_usart_2();

	// Interrupt für USART aktivieren
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	NVIC_EnableIRQ(USART2_IRQn);

	aufgabe_6_aktiv = part + 1;

	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
	if (aufgabe_6_aktiv == 2)
		set_frequent_alarm();
	else if (aufgabe_6_aktiv == 3)
		set_monday_alarm();
	else
		return;

	init_alarm_interrupt();

	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	char buffer[50];

	while (1) {
		if (!setting_time) {
			RTC_GetDate(RTC_Format_BIN, &date);
			RTC_GetTime(RTC_Format_BIN, &time);
			sprintf(buffer,
					"Heute ist %s der %02i.%02i.%02i. Es ist %02i:%02i:%02i Uhr.\r\n",
					WEEKDAYS[date.RTC_WeekDay - 1], date.RTC_Date,
					date.RTC_Month, date.RTC_Year, time.RTC_Hours,
					time.RTC_Minutes, time.RTC_Seconds);
			usart_2_print(buffer);
		}

		wait_sec(1);
	}
}

void time_alarm(int second_offset) {
	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
	// Anlegen der Structs für aktuelle Daten
	RTC_AlarmTypeDef RTC_Alarm_Aktuell; //  Alarm
	RTC_TimeTypeDef cur_time;
	RTC_GetTime(RTC_Format_BIN, &cur_time);

	RTC_Alarm_Aktuell.RTC_AlarmTime.RTC_Hours = 0x00;
	RTC_Alarm_Aktuell.RTC_AlarmTime.RTC_Minutes = 0x00;
	RTC_Alarm_Aktuell.RTC_AlarmTime.RTC_Seconds = (cur_time.RTC_Seconds
			+ second_offset) % 60;

	// Alarmmaske setzen kann auch verodert werden
	RTC_Alarm_Aktuell.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay // Wochentag oder Tag ausgeblendet
	| RTC_AlarmMask_Hours // Stunde ausgeblendet
			| RTC_AlarmMask_Minutes; // Minute ausgeblendet

	// Alarm für den Tag oder Wochentag auswählen
	RTC_Alarm_Aktuell.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date; // Tag (1-31)
	// Alarm Tag oder Wochentag setzen
	RTC_Alarm_Aktuell.RTC_AlarmDateWeekDay = 0x01; // Tag 0x01...0x31

	RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_Alarm_Aktuell);
	RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
}

void aufgabe6_2_2() {
	// Real Time Clock initialisieren
	start_RTC();

	// LEDs initialisieren
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_leds();

	// USART initialisieren
	init_usart_2();

	// Interrupt für USART aktivieren
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	NVIC_EnableIRQ(USART2_IRQn);

	aufgabe_6_aktiv = 4;

	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

	init_alarm_interrupt();
	time_alarm(25);

	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	char buffer[50];

	while (1) {
		if (!setting_time) {
			RTC_GetDate(RTC_Format_BIN, &date);
			RTC_GetTime(RTC_Format_BIN, &time);
			sprintf(buffer,
					"Heute ist %s der %02i.%02i.%02i. Es ist %02i:%02i:%02i Uhr.\r\n",
					WEEKDAYS[date.RTC_WeekDay - 1], date.RTC_Date,
					date.RTC_Month, date.RTC_Year, time.RTC_Hours,
					time.RTC_Minutes, time.RTC_Seconds);
			usart_2_print(buffer);
		}

		wait_sec(1);
	}
}

void print_alarm_info() {
	char buffer[50];
	RTC_TimeTypeDef time;
	RTC_GetTime(RTC_Format_BIN, &time);
	// Für Aufgabe6 = 2 oder 4
	if (aufgabe_6_aktiv % 2 == 0) {
		sprintf(buffer, "Es ist %02i:%02i:%02i Uhr.\r\n", time.RTC_Hours,
				time.RTC_Minutes, time.RTC_Seconds);
	} else if (aufgabe_6_aktiv == 3) {
		RTC_DateTypeDef date;
		RTC_GetDate(RTC_Format_BIN, &date);
		sprintf(buffer,
				"ALARM!!!! %02i.%02i.%02i. Es ist %02i:%02i:%02i Uhr.\r\n",
				date.RTC_Date, date.RTC_Month, date.RTC_Year, time.RTC_Hours,
				time.RTC_Minutes, time.RTC_Seconds);
	}
	usart_2_print(buffer);
}

/** 
* Berechnet zu einem gegebenen Datum den Wochentag.
* Montag = 1, Sonntag = 7
* Quelle: https://de.wikipedia.org/wiki/Wochentagsberechnung
*/
uint8_t calc_week_day(uint8_t day, uint8_t month, uint8_t year) {
	uint8_t N_T = day % 7;
	uint8_t N_Ms[] = { 0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5 };
	uint8_t N_M = N_Ms[month - 1];
	uint8_t N_Jz = (year + (year / 4)) % 7;
	uint8_t N_Jh = 6; // Hardgecoded für das 21. Jhd.
	uint8_t full_year = 2000 + year;
	int is_sj = ((full_year % 4 == 0) && (full_year % 100 != 0))
			|| (full_year % 400 == 0);
	uint8_t N_Sj = (is_sj && month < 3) ? -1 : 0;
	uint8_t temp = (N_T + N_M + N_Jh + N_Jz + N_Sj) % 7;
	return (temp == 0) ? 7 : temp;
}

int update_date_time(char* date_time_string) {
	// Parse Datum und Zeit mit Format d.M.y h:m:s
	unsigned int day, month, year, hours, minutes, seconds;
	int read = sscanf(date_time_string, "%u.%u.%u %u:%u:%u", &day, &month,
			&year, &hours, &minutes, &seconds);

	// Falls nicht alle Parameter zugewiesen -> Fehler
	if (read != 6)
		return ERROR;

	// Datum Werte zuweisen
	RTC_DateTypeDef new_date;

	new_date.RTC_Date = (uint8_t) day;
	new_date.RTC_Month = (uint8_t) month;
	new_date.RTC_Year = (uint8_t) year;
	new_date.RTC_WeekDay = calc_week_day(new_date.RTC_Date, new_date.RTC_Month,
			new_date.RTC_Year);

	// Datum setzen
	if (RTC_SetDate(RTC_Format_BIN, &new_date) == ERROR)
		return ERROR;

	// Zeit Werte zuweisen
	RTC_TimeTypeDef new_time;

	new_time.RTC_Hours = (uint8_t) hours;
	new_time.RTC_Minutes = (uint8_t) minutes;
	new_time.RTC_Seconds = (uint8_t) seconds;

	// Zeit setzen
	return RTC_SetTime(RTC_Format_BIN, &new_time);
}

