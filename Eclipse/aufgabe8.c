#include "aufgabe8.h"

void init_tim7_interrupt(unsigned long hz) {
	// Konfiguration der Interruptcontrollers
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Taktsystem für TIM7 Freigeben
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);

	// Struktur anlegen
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	// TIM7 in der Struktur konfigurieren
	TIM_TimeBaseStructure.TIM_Prescaler = 8400 - 1; // 100us = 8400 * 1/84000000Hz
	TIM_TimeBaseStructure.TIM_Period = (10000 / hz) - 1; // 1s = 10000 * 100us
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	// TIM7 Register aus der Struktur Schreiben
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);

	// TIM7 Interrupt erlauben
	TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);

	// TIM 7 Freigeben (Takt auf Counter schalten)
	TIM_Cmd(TIM7, ENABLE);

	// ab jetzt wird jede Sekunde ein Interrupt ausgelöst
	// der die ISR d.h. den TIM7_IRQHandler aufruft.
}

void aufgabe8_1_1() {
	aufgabe_8_aktiv = 1;
	timer_counter = 0;
	init_usart_2_tx();
	init_tim7_interrupt(1);
	while (1) {
	}
}

void aufgabe8_1_2() {
	aufgabe_8_aktiv = 2;

	// Taktquellen aktivieren
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	// Initialisieren
	init_usart_2_tx();
	init_taste_1();
	init_taste_2();

	init_tim7_interrupt(100);
	while (1) {
		// Warte bis Taste 1 gedrückt
		while (!read_taste_1()) {
		}
		// Starte Zeitmessung

		timer_counter = 0;

		unsigned long time_start = timer_counter;

		while (!read_taste_2()) {
		}

		// Stopwert der Zeitmessung
		unsigned long time_stop = timer_counter;

		// Berechnung der Zeitdauer
		uint32_t time_dauer = time_stop - time_start;
		sprintf(usart2_tx_buffer, "Es wurde eine Zeit von %lu ms gemessen.\r\n",
				time_dauer * 10);
		usart_2_print(usart2_tx_buffer);
	}
}

void init_tim_5() {
	// Struktur anlegen
	GPIO_InitTypeDef GPIO_InitStructure;
	// Struktur Initialisieren
	GPIO_StructInit(&GPIO_InitStructure);
	// Portleitung in der Struktur Konfigurieren
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	// Werte aus der Struktur in die Register schreiben
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Alternativfunktion der Portleitung Freigeben
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_TIM5);

	// Taktsystem für Timer TIM5 Freigeben
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

	// Erkennen steigender Flanke an TI1
	TIM5->CCMR1 |= TIM_CCMR1_CC1S_0;

	// Polarität
	TIM5->CCER |= TIM_CCER_CC1P;

	// Slave Mode, external Clock Mode1, TI1FP1 Signalquelle
	TIM5->SMCR |= TIM_SMCR_SMS + TIM_SMCR_TS_2 + TIM_SMCR_TS_0;

	// Konfiguration der Interruptcontrollers
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Struktur anlegen
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	// TIM5 in der Struktur konfigurieren
	TIM_TimeBaseStructure.TIM_Prescaler = 1;
	TIM_TimeBaseStructure.TIM_Period = 10 - 1; // Grenzwert Tastenbetätigungen
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	// TIM5 Register aus der Struktur Schreiben
	TIM_TimeBaseInit(TIM5, &TIM_TimeBaseStructure);

	// TIM5 Interrupt erlauben
	TIM_ITConfig(TIM5, TIM_IT_Update, ENABLE);

	// Counter auf 0 setzen
	TIM_SetCounter(TIM5, 0);

	// Timer TIM5 Freigeben
	TIM_Cmd(TIM5, ENABLE);

	// ab jetzt führt jede Tastenbetätigung an Taster1 zur Erhöhung des
	// CNT-Counter Wertes von Timer TIM5
}

void aufgabe8_1_3() {
	aufgabe_8_aktiv = 3;

	// Taktquellen aktivieren
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// Initialisieren
	init_usart_2_tx();

	init_tim_5();
	while (1) {
		wait_sec(2);
		sprintf(usart2_tx_buffer, "Taste 2 wurde %lu mal gedrückt.\r\n",
				TIM_GetCounter(TIM5));
		usart_2_print(usart2_tx_buffer);
	}
}

void play_led_intro() {
	RT_LED_OFF;
	GB_LED_OFF;
	GR_LED_OFF;

	RT_LED_ON;
	wait_sec(1);

	GB_LED_ON;
	wait_sec(1);

	GR_LED_ON;
	wait_sec(1);

	RT_LED_OFF;
	GB_LED_OFF;
	GR_LED_OFF;
}

void toggle_led(char led) {
	if (led == 0)
		RT_LED_TOGGLE;
	else if (led == 1)
		GB_LED_TOGGLE;
	else
		GR_LED_TOGGLE;
}

uint32_t random(void) {
	uint32_t zahl = 0;

	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);
	RNG_Cmd(ENABLE);

	while (RNG_GetFlagStatus(RNG_FLAG_DRDY) == RESET)
		;
	zahl = RNG_GetRandomNumber();

	RNG_Cmd(DISABLE);
	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, DISABLE);

	return zahl;
}

void aufgabe8_1_4() {
	aufgabe_8_aktiv = 4;

	// Taktquellen aktivieren
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	// Initialisieren
	init_usart_2();
	init_taste_1();
	init_leds();

	init_tim7_interrupt(100);
	while (1) {
		if (usart_2_read() != 's')
			continue;
		// Spielt ein kleines Intro, um zu signalisieren
		// wann es los geht
		play_led_intro();

		unsigned long min = 1000000;
		unsigned long max = 0;
		unsigned long total = 0;
		int i;
		for (i = 0; i < 10; i++) {
			// Wähle eine zufällige LED
			int led = random() % 3;

			// Warte 2 - 10 Sekunden
			wait_sec((random() % 9) + 2);

			// Schalte die LED an
			toggle_led(led);

			timer_counter = 0;

			// Starte die Zeitmessung
			unsigned long time_start = timer_counter;

			// Warte auf die Reaktion (Tastendruck)
			while (!read_taste_1()) {
			}

			// Stoppe die Zeitmessung
			unsigned long time_stop = timer_counter;

			unsigned long time_dauer = 10 * (time_stop - time_start);

			// Schalte die LED wieder aus
			toggle_led(led);

			// Checke Maximal -und Minimalwert
			if (time_dauer > max)
				max = time_dauer;
			else if (time_dauer < min)
				min = time_dauer;

			// Addiere auf die Gesamtzeit
			total += time_dauer;
		}
		// Durchschnitt berechnen
		double average = total / 10.0;

		// Statistik ausgeben
		sprintf(usart2_tx_buffer,
				"\r\nMinimum: %lu ms\r\nMaximum: %lu ms\r\nDurchschnitt: %lf ms\r\n",
				min, max, average);
		usart_2_print(usart2_tx_buffer);
	}
}

// Initialisiert PB09 im Alternativmodus
void init_pwm() {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_ResetBits(GPIOB, GPIO_Pin_9);

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_TIM11);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM11, ENABLE);
}


void init_pwm_signal_timer(float frequency) {
	disable_pwm_timer();

	int CL_INT = 168000000;
	int vorteiler = 2100;
	// Periode anhand der Frequenz berechnen
	int periode = (int) (CL_INT / vorteiler / frequency);

	// Timer mit den Werten initialisieren
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_TimeBaseStructure.TIM_Prescaler = vorteiler - 1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_Period = periode - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;

	TIM_TimeBaseInit(TIM11, &TIM_TimeBaseStructure);

	// PWM initialisieren
	TIM_OCInitTypeDef TIM_OCInitStructure;
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = (unsigned int) (periode / 2);
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OC1Init(TIM11, &TIM_OCInitStructure);

	TIM_OC1PreloadConfig(TIM11, TIM_OCPreload_Enable);

	TIM_ARRPreloadConfig(TIM11, ENABLE);
	// Timer 11 wird freigegeben
	TIM_Cmd(TIM11, ENABLE);
}

// Timer 11 deaktivieren (Signal ausschalten)
void disable_pwm_timer() {
	TIM_Cmd(TIM11, DISABLE);
}

// Noten-Repräsentation des Musikstücks "The Imperial March"
// Das Array enthält für jeden Takt ein Array mit 16tel Noten
// Mit NOTE_C, NOTE_RING ... kann eine längere Note erzeugt werden
float imperial_march[12][16] = { { NOTE_G, NOTE_RING, NOTE_RING, NOTE_RING,
			NOTE_G, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_G, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_DIS, NOTE_RING, NOTE_RING, NOTE_AIS }, { NOTE_G,
			NOTE_RING, NOTE_RING, NOTE_RING, NOTE_DIS, NOTE_RING, NOTE_RING,
			NOTE_AIS, NOTE_G, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_RING }, { NOTE_d, NOTE_RING, NOTE_RING, NOTE_RING,
			NOTE_d, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_d, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_dis, NOTE_RING, NOTE_RING, NOTE_AIS }, { NOTE_FIS,
			NOTE_RING, NOTE_RING, NOTE_RING, NOTE_DIS, NOTE_RING, NOTE_RING,
			NOTE_AIS, NOTE_G, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_RING }, { NOTE_g, NOTE_RING, NOTE_RING, NOTE_RING,
			NOTE_G, NOTE_RING, NOTE_RING, NOTE_G, NOTE_g, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_fis, NOTE_RING, NOTE_RING, NOTE_f }, { NOTE_e, NOTE_dis,
			NOTE_e, NOTE_RING, NOTE_PAUSE, NOTE_PAUSE, NOTE_GIS, NOTE_RING,
			NOTE_cis, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_c, NOTE_RING, NOTE_RING,
			NOTE_H }, { NOTE_AIS, NOTE_A, NOTE_AIS, NOTE_RING, NOTE_PAUSE,
			NOTE_PAUSE, NOTE_DIS, NOTE_RING, NOTE_FIS, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_DIS, NOTE_RING, NOTE_RING, NOTE_FIS }, { NOTE_AIS,
			NOTE_RING, NOTE_RING, NOTE_RING, NOTE_G, NOTE_RING, NOTE_RING, NOTE_AIS,
			NOTE_d, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_RING }, { NOTE_g, NOTE_RING, NOTE_RING, NOTE_RING,
			NOTE_G, NOTE_RING, NOTE_RING, NOTE_G, NOTE_g, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_fis, NOTE_RING, NOTE_RING, NOTE_f }, { NOTE_e, NOTE_dis,
			NOTE_e, NOTE_RING, NOTE_PAUSE, NOTE_PAUSE, NOTE_GIS, NOTE_RING,
			NOTE_cis, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_c, NOTE_RING, NOTE_RING,
			NOTE_H }, { NOTE_AIS, NOTE_A, NOTE_AIS, NOTE_RING, NOTE_PAUSE,
			NOTE_PAUSE, NOTE_DIS, NOTE_RING, NOTE_FIS, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_DIS, NOTE_RING, NOTE_RING, NOTE_AIS }, { NOTE_G,
			NOTE_RING, NOTE_RING, NOTE_RING, NOTE_DIS, NOTE_RING, NOTE_RING,
			NOTE_AIS, NOTE_G, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_RING, NOTE_RING,
			NOTE_RING, NOTE_RING } };

// Spielt ein Musikstück aus obigen Format ab
void play_music(float bpm, int measure_count, int measure_entry_count,
		float notes[measure_count][measure_entry_count]) {
	init_pwm();
	int measure = 0;
	// Berechne die Zeit eines Beats aus den Beats per Minute in us
	int beat_time = (int) ((1.0 / (bpm / 60000000.0))
			/ (measure_entry_count / 4));
	// Gehe alle Takte durch
	while (measure < measure_count) {
		int i;
		for (i = 0; i < measure_entry_count; i++) {
			// Bei einer Pause deaktiviere das Signal
			if (notes[measure][i] == NOTE_PAUSE) {
				disable_pwm_timer();
			// Bei einer Note, spiele die entsprechende Frequenz
			} else if (notes[measure][i] != NOTE_RING) {
				init_pwm_signal_timer(notes[measure][i]);
			}
			// Bei einem NOTE_RING wird einfach der aktuelle Ton fortgesetzt

			// Warte eine Beat Zeit ab
			wait_uSek(beat_time);
		}

		measure++;
	}
	disable_pwm_timer();
}

void aufgabe8_2_1() {
	init_usart_2();
	init_pwm();
	while (1) {
		float frequency;
		char c = usart_2_read();
		if (c == 'q') {
			frequency = 261.626;
		} else if (c == 'w') {
			frequency = 293.665;
		} else if (c == 'e') {
			frequency = 329.628;
		} else if (c == 'r') {
			frequency = 349.228;
		} else if (c == 't') {
			frequency = 391.995;
		} else if (c == 'z') {
			frequency = 440;
		} else if (c == 'u') {
			frequency = 493.883;
		} else if (c == 'i') {
			frequency = 523.251;
		} else if (c == 'o') {
			frequency = 587.330;
		} else if (c == '2') {
			frequency = 277.183;
		} else if (c == '3') {
			frequency = 311.127;
		} else if (c == '5') {
			frequency = 369.994;
		} else if (c == '6') {
			frequency = 415.305;
		} else if (c == '7') {
			frequency = 466.164;
		} else if (c == '\r') {
			TIM_Cmd(TIM11, DISABLE);
			continue;
		} else if (c == 'm') {
			play_music(104, 12, 16, imperial_march);
			continue;
		} else {
			continue;
		}
		init_pwm_signal_timer(frequency);
	}
}

void init_pwm_led() {
	// Taktsystem für Port GPIOB freigeben
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	// Struktur anlegen
	GPIO_InitTypeDef GPIO_InitStructure;

	// Struktur mit Konfiguration laden
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	// Konfiguration aus der Struktur in Register laden
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	// Alternativ Nutzung auswählen
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_TIM12);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_TIM12);
}

void init_pwm_timer_led(int tastverhaeltnis1, int tastverhaeltnis2) {
	uint16_t PWM_Periode = 42; // Periodendauer
	uint16_t PSC_Prescaler = 84; // Prescaler
	uint16_t PWM_Tastverhaeltnis_OC1 = tastverhaeltnis1; // Tastverhältnis TIM12C1 in %
	uint16_t PWM_Tastverhaeltnis_OC2 = tastverhaeltnis2; // Tastverhältnis TIM12C2 in %
	// Pulsbreiten werden später berechnet
	uint16_t PWM_Pulsbreite_OC1 = 0;
	uint16_t PWM_Pulsbreite_OC2 = 0;

	// Periode von 20 ms
	PWM_Periode = (uint16_t)(0.020f / 0.000001f);

	// Prozentuales Berechnen der Pulsbreiten
	PWM_Pulsbreite_OC1 = (uint16_t)(
			PWM_Periode * PWM_Tastverhaeltnis_OC1 / 100);
	PWM_Pulsbreite_OC2 = (uint16_t)(
			PWM_Periode * PWM_Tastverhaeltnis_OC2 / 100);

	// Freigabe des Taktsystems für den Timer TIM12
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM12, ENABLE);

	// Anlegen der Struktur
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	//Konfigurationsdaten in die Struktur schreiben
	TIM_TimeBaseStructure.TIM_Prescaler = PSC_Prescaler - 1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_Period = PWM_Periode - 1; //ARR
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;

	// Konfugurationsdaten in die Register schreiben
	TIM_TimeBaseInit(TIM12, &TIM_TimeBaseStructure);

	// Anlegen der Struktur für den Output Channel
	TIM_OCInitTypeDef TIM_OCInitStructure;

	// Konfigurationsdaten in die Strukrur
	// für den Output Channel eintragen
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;

	TIM_OCInitStructure.TIM_Pulse = PWM_Pulsbreite_OC1; //CCR

	// Konfigurationsdaten in die Register schreiben
	TIM_OC1Init(TIM12, &TIM_OCInitStructure); // OutputChannel 1
	// Konfigurationsdaten in die Register schreiben
	TIM_OCInitStructure.TIM_Pulse = PWM_Pulsbreite_OC2; //CCR

	TIM_OC2Init(TIM12, &TIM_OCInitStructure); // Outputchannel 2

	// Preload Register freigeben
	TIM_OC1PreloadConfig(TIM12, TIM_OCPreload_Enable);
	TIM_OC2PreloadConfig(TIM12, TIM_OCPreload_Enable);

	// Preload Register freigeben
	TIM_ARRPreloadConfig(TIM12, ENABLE);

	// Taktsignal für den Timer freigeben
	TIM_Cmd(TIM12, ENABLE);
}

// Eine Sinusfunktion, welche die LEDs pulsieren lässt
int led_oscillation_func(double time) {
	return 20 * sin(M_PI * time) + 20;
}

void aufgabe8_2_2() {
	// Initialisierung der LEDs für PWM
	init_pwm_led();

	double time = 0;
	while (1) {
		// Ändert die Intensität der LEDs mittels der Sinusfunktion
		init_pwm_timer_led(led_oscillation_func(time),
				led_oscillation_func(time - 1));
		wait_uSek(1000);
		time += 0.001;
	}
}

// Initialisierung der Ports und anschalten der Clock
void init_pwm_motor() {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_ResetBits(GPIOB, GPIO_Pin_8);

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_TIM10);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM10, ENABLE);
}

/**
 * @param pulse_width pulse width in us
 */
void init_pwm_timer_motor(uint16_t pulse_width) {
	// benötigte Variablen
	uint16_t PWM_Periode = 0; // Periodendauer
	uint16_t PSC_Prescaler = 168; // Prescaler

	// Periode von 20 ms
	PWM_Periode = (uint16_t)(0.020f / 0.000001f);

	// Freigabe des Taktsystems für den Timer TIM10
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM10, ENABLE);

	// Anlegen der Struktur
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	//Konfigurationsdaten in die Struktur schreiben
	TIM_TimeBaseStructure.TIM_Prescaler = PSC_Prescaler - 1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_Period = PWM_Periode - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;

	// Konfugurationsdaten in die Register schreiben
	TIM_TimeBaseInit(TIM10, &TIM_TimeBaseStructure);

	// Anlegen der Struktur für den Output Channel
	TIM_OCInitTypeDef TIM_OCInitStructure;

	// Konfigurationsdaten in die Strukrur
	// für den Output Channel eintragen
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OCInitStructure.TIM_Pulse = pulse_width;

	// Konfigurationsdaten in die Register schreiben
	TIM_OC1Init(TIM10, &TIM_OCInitStructure); // OutputChannel 1

	// Preload Register freigeben
	TIM_OC1PreloadConfig(TIM10, TIM_OCPreload_Enable);

	// Preload Register freigeben
	TIM_ARRPreloadConfig(TIM10, ENABLE);

	// Taktsignal für den Timer freigeben
	TIM_Cmd(TIM10, ENABLE);
}

void aufgabe8_2_3() {
	init_usart_2();
	// Maximale Pulsweiten für die Konfiguration des Servos
	uint16_t min_width = 850;
	uint16_t max_width = 2400;
	// Initiale Pulsweite
	uint16_t pulse_width = 1500;
	// Initialisierung des Ports und der Clocks
	init_pwm_motor();
	while (1) {
		// Verstellen des Servos auf die gewünschte Pulsweite
		init_pwm_timer_motor(pulse_width);
		// Lesen eines Buchstabes vom "Terra Term"
		char input = usart_2_read();
		// Verändern der Pulsweite je nach Eingabe
		if (input == '+') {
			pulse_width += 50;
		} else if (input == '-') {
			pulse_width -= 50;
		}
		// Beschränkung der Pulsweite
		pulse_width = (pulse_width > max_width)? max_width : pulse_width;
		pulse_width = (pulse_width < min_width)? min_width : pulse_width;
	}
}