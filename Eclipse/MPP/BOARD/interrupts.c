#include "interrupts.h"

uint16_t A_wert = 0;
uint16_t B_wert = 0;
uint16_t start_wert = 0;
uint16_t end_wert = 0;
uint16_t messung_der_periodendauer = 0;
uint32_t differenz_wert = 0;

int32_t timer = 0;
int32_t Tas1 = 0;
int32_t Tas2 = 0;
int16_t T1 = 0;
int16_t T2 = 0;
char tasten[50];

float frequenz = 0;
float periodendauer = 0;
char bufi[40];

char string_out[50];
char string_in[50];
uint16_t j = 0;

char dma_print_buffer[1024];

char taster1_pressed_count = 0;
char taster2_pressed_count = 0;

int usart2_in_length = 0; // Länge der aktuellen Eingabe über USART

// Private Funktionen
// ========================================================================

void print_tasten_status(int i) {
	sprintf(usart2_tx_buffer, "Taste %d gedrückt. #Taste1: %d, #Taste2: %d\r\n",
			i, taster1_pressed_count, taster2_pressed_count);
	usart_2_print(usart2_tx_buffer);
}

void TASTER1_IRQ() {
	if (aufgabe_5_aktiv == 3) {
		taster1_pressed_count++;
		if (taster1_pressed_count == 10) {
			disable_taste1_irq();
			taster2_pressed_count = 0;
		}
		print_tasten_status(1);
	}
	if (aufgabe_5_aktiv)
		GB_LED_TOGGLE;

	if (aufgabe_12_aktiv == 3) {
		taster_callback(1);
	}

	if (aufgabe_12_aktiv == 6) {
		strcpy(usart2_rx_buffer, "Taste 1 gedrückt.\r\n");
		send_buffer = 1;
	}
}

void TASTER2_IRQ() {
	if (aufgabe_5_aktiv == 3) {
		if (taster1_pressed_count == 10) {
			taster2_pressed_count++;
			if (taster2_pressed_count == 2) {
				taster1_pressed_count = 0;
				enable_taste1_irq(EXTI_Trigger_Rising);
			}
		}
		print_tasten_status(2);
	}

	if (aufgabe_5_aktiv)
		GR_LED_TOGGLE;

	if (aufgabe_7_aktiv) {
		GB_LED_OFF;
	}

	if (aufgabe_12_aktiv == 3) {
		taster_callback(2);
	}

	if (aufgabe_12_aktiv == 5 && !setting_time) {
		set_ntp_time = 1;
	}

	if (aufgabe_12_aktiv == 6) {
		strcpy(usart2_rx_buffer, "Taste 2 gedrückt.\r\n");
		send_buffer = 1;
	}
}

// Interrupt Handler
// ========================================================================

void hard_fault_handler_c(unsigned int * hardfault_args);

//=========================================================================
void NMI_Handler(void) {
}

//=========================================================================
void HardFault_Handler(void) {
	asm ("TST LR, #4");
	asm ("ITE EQ");
	asm ("MRSEQ R0, MSP");
	asm ("MRSNE R0, PSP");
	asm ("B hard_fault_handler_c");
}

//=========================================================================
void MemManage_Handler(void) {
	while (1) {
		;
	}
}

//=========================================================================
void BusFault_Handler(void) {
	while (1) {
		;
	}
}

//=========================================================================
void UsageFault_Handler(void) {
	while (1) {
		;
	}
}

//=========================================================================
void SVC_Handler(void) {
}

//=========================================================================
void DebugMon_Handler(void) {
}

//=========================================================================
//void PendSV_Handler(void)
//{
//}

//=========================================================================
void SysTick_Handler(void) {
	static unsigned long SysTickCounter0 = 0;
	static unsigned long SysTickCounter1 = 0;
	static unsigned long SysTickCounter2 = 0;
	static unsigned long SysTickPeriode = 0;

	SysTickCounter0++;
	SysTickCounter1++;
	SysTickCounter2++;

//    //CoOS_SysTick_Handler(); //CoOs arch.c
//    CoOS_SysTick_Handler();
//
//    // CC3000 alle 50ms CC3000_select() aufrufen
	if (SysTickCounter2 >= 5) {
		SysTickCounter2 = 0;
		if (CC3000_DHCP_done == true && CC3000_is_Connected == true) {
			CC3000_select();
		}
	}
//
//    // SDCARD alle 10ms Timer f�r Wartezeiten runterz�hlen
//    //SDCARD_SysTick_Handler();

	if (aufgabe_2_aktiv) {
		if (!SysTickPeriode && SysTickCounter0 == 300) {
			RT_LED_ON;
			SysTickPeriode = 1;
			SysTickCounter0 = 0;
		} else if (SysTickPeriode && SysTickCounter0 == 50) {
			RT_LED_OFF;
			SysTickPeriode = 0;
			SysTickCounter0 = 0;
		}
	}

	if (SysTickCounter1 == 10) {
		timer--;
		SysTickCounter1 = 0;
	}

}

//=========================================================================
void WWDG_IRQHandler(void) {
	WWDG_ClearFlag();
	usart_2_print("WWDG_IRQn \r\n");
}

//=========================================================================
void EXTI0_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line0) == SET) {
		EXTI_ClearFlag(EXTI_Line0);
		EXTI_ClearITPendingBit(EXTI_Line0);
		TASTER2_IRQ();
	}
}

//=========================================================================
void EXTI1_IRQHandler(void) {

}

//=========================================================================
void EXTI2_IRQHandler(void) {

}

//=========================================================================
void EXTI3_IRQHandler(void) {

}

//=========================================================================
void EXTI4_IRQHandler(void) {

}

//=========================================================================
void EXTI9_5_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line5) == SET) {
		ISR_IRQ5_CC1101();
	}
	if (EXTI_GetITStatus(EXTI_Line6) == SET) {
		//uart_send("6 ");
	}
	if (EXTI_GetITStatus(EXTI_Line7) == SET) {
		uart_send("7 ");
	}
	if (EXTI_GetITStatus(EXTI_Line8) == SET) {
		//ISR_IRQ8_TAC6416();
	}
	if (EXTI_GetITStatus(EXTI_Line9) == SET) {
		//uart_send("9 ");
	}
}

//=========================================================================
void EXTI15_10_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line10) == SET) {
		//uart_send("W\r\n");
		WLAN_IRQ_Handler();
	}

	if (EXTI_GetITStatus(EXTI_Line13) == SET) {
		EXTI_ClearFlag(EXTI_Line13);
		EXTI_ClearITPendingBit(EXTI_Line13);
		TASTER1_IRQ();
	}
}

//=========================================================================
void RTC_Alarm_IRQHandler(void) {
	if (RTC_GetITStatus(RTC_IT_ALRA) != RESET) {
		RTC_ClearITPendingBit(RTC_IT_ALRA);
		EXTI_ClearITPendingBit(EXTI_Line17);

		if (aufgabe_6_aktiv)
			print_alarm_info();
		if (aufgabe_6_aktiv == 4) {
			GB_LED_TOGGLE;
			time_alarm(25);
		}

		if (aufgabe_12_aktiv == 4) {
			RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
			init_alarm_interrupt();
			time_alarm(300);
			// Clear PWR Wakeup WUF Flag
			PWR_ClearFlag(PWR_CSR_WUF);
			// Enable RTC Wakeup Interrupt
			RTC_ITConfig(RTC_IT_WUT, ENABLE);
			PWR_WakeUpPinCmd(ENABLE);

			// Enter Standby mode
			PWR_EnterSTANDBYMode();
		}

	}
}

//=========================================================================
void ADC_IRQHandler(void) {
	if (ADC_GetITStatus(ADC1, ADC_IT_EOC) == SET) {
		// ... Code für Ende Wandlung
		//ADC_ClearITPendingBit(ADC1, ADC_IT_EOC);
	}
	if (ADC_GetITStatus(ADC1, ADC_IT_AWD) == SET) {
		// ... Code für analogen Watchdog
		//ADC_ClearITPendingBit(ADC1, ADC_IT_AWD);
	}
}

//=========================================================================
/**
 * Liest das übergebene Zeichen in den Empfangspuffer.
 * Gibt 0 zurück, wenn noch kein \r gelesen wurde bzw.
 * der Puffer noch genug Platz hat. Gibt die Länge der
 * gelesenen Zeichenkette zurück, wenn die Eingabe beendet
 * wurde. Die Zeichenkette steht dann in usart2_rx_buffer.
 */
int read_usart_string(char c) {
	int read_buffer_size = sizeof(usart2_rx_buffer);

	if (c == '\r' || usart2_in_length == read_buffer_size - 1) {
		// Terminiere die Zeichenkette mit 0-Byte
		usart2_rx_buffer[usart2_in_length] = '\0';

		if (usart2_in_length == read_buffer_size - 1) {
			usart_2_print("\r\n");
		}

		int length = usart2_in_length;
		usart2_in_length = 0;
		return length;
	} else if (c == 0x08) { // Wenn Backspace
		// Gehe ein Zeichen zurück
		if (usart2_in_length > 0)
			usart2_in_length--;
	} else if (c != '\n') {
		usart2_rx_buffer[usart2_in_length++] = c;
	}

	return 0;
}

void USART2_IRQHandler(void) {
	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET) {
		char c = (char) USART_ReceiveData(USART2);
		USART_ClearITPendingBit(USART2, USART_IT_RXNE);
		if (aufgabe_5_aktiv == 4) {
			if (c == '1') {
				GB_LED_TOGGLE;
			} else if (c == '2') {
				GR_LED_TOGGLE;
			} else if (c == '\r') {
				GB_LED_OFF;
				GR_LED_OFF;
			}
		} else if (aufgabe_5_aktiv == 5) {
			int length = read_usart_string(c);
			if (length != 0) {
				usart_2_print(usart2_rx_buffer);
				sprintf(usart2_tx_buffer, ": %d\r\n", length);
				usart_2_print(usart2_tx_buffer);
			}
		} else if (aufgabe_6_aktiv || aufgabe_12_aktiv == 5) {
			// Strg + T gedrückt
			if (c == 0x14) {
				setting_time = 1;
				usart_2_print(
						"\r\nSetze das Datum neu (Format: \"d.M.y h:m:s\")\r\n");
			} else if (setting_time) {
				if (read_usart_string(c)) {
					int success = update_date_time(usart2_rx_buffer);
					if (success) {
						usart_2_print(
								"Datum und Uhrzeit erfolgreich gesetzt.\r\n");
					} else {
						usart_2_print(
								"Datum und Uhrzeit konnten nicht gesetzt werden.\r\n");
					}

					setting_time = 0;
				}
			}
		} else if (aufgabe_11_aktiv) {
			int length = read_usart_string(c);
			if (length != 0) {
				sprintf(dma_print_buffer, "%s: %d\r\n", usart2_rx_buffer,
						length);
				print_usart2_dma(dma_print_buffer);
			}
		} else if (aufgabe_12_aktiv == 6) {
			int length = read_usart_string(c);
			if (length != 0) {
				send_buffer = 1;
			}
		}
	}
}

//=========================================================================
void USART6_IRQHandler(void) {

}

//=========================================================================

void DMA1_Stream6_IRQHandler(void) {
	// ist der DMA Stream komplett?
	if (DMA_GetITStatus(DMA1_Stream6, DMA_IT_TCIF4)) {
		// dann wird Transfer Complete interrupt pending bit zurückgesetzt
		DMA_ClearITPendingBit(DMA1_Stream6, DMA_IT_TCIF4);
	}
}

//=========================================================================
void RTC_WKUP_IRQHandler(void) {
	if (RTC_GetITStatus(RTC_IT_WUT) != RESET) {
		LED_GR_TOGGLE;
		RTC_ClearITPendingBit(RTC_IT_WUT);
		EXTI_ClearITPendingBit(EXTI_Line22);
	}
}

//=========================================================================
void TIM5_IRQHandler(void) {
	if (TIM_GetITStatus(TIM5, TIM_IT_Update) != RESET) {
		TIM_ClearITPendingBit(TIM5, TIM_IT_Update);
		if (aufgabe_8_aktiv == 3) {
			usart_2_print("Taste 10 mal gedrückt. Counter Reset!\r\n");
		}
	}
}

//=========================================================================
void TIM7_IRQHandler(void) {
	if (TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET) {
		TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
		if (aufgabe_8_aktiv == 1) {
			timer_counter++;
			sprintf(usart2_tx_buffer, "Timer: %lu\r\n", timer_counter);
			usart_2_print(usart2_tx_buffer);
		} else if (aufgabe_8_aktiv > 0 && aufgabe_8_aktiv % 2 == 0) {
			timer_counter++;
		} else if (aufgabe_10_aktiv == 1) {
			// Setze die korrekte Spannung und erhöhe die x-Position
			set_output_voltage(sinus_voltage[sinus_x_pos++], 12);
			sinus_x_pos %= 100;
		} else if (aufgabe_10_aktiv == 2) {
			// Eingabespannung messen
			float voltage = get_measure_value(TYPE_EXTERN_VOLTAGE);
			// Zyklischer Buffer
			average_values[average_value_index++] = voltage;
			average_value_index %= average_window_width;
			// Durchschnitt berechnen
			float average = 0;
			int i;
			for (i = 0; i < average_window_width; i++) {
				average += average_values[i];
			}
			average /= average_window_width;
			// Ausgabespannung setzen
			set_output_voltage(average, 12);
		} else if (aufgabe_12_aktiv == 4 || aufgabe_12_aktiv == 5
				|| aufgabe_12_aktiv == 6) {
			if (color_toggle == 1)
				GB_LED_TOGGLE;
			else if (color_toggle == 2)
				RT_LED_TOGGLE;
		}
	}
}

//=========================================================================
void TIM6_DAC_IRQHandler() {

}

//=========================================================================
// From Joseph Yiu, minor edits by FVH
// hard fault handler in C,
// with stack frame location as input parameter
// called from HardFault_Handler in file xxx.s
void hard_fault_handler_c(unsigned int * hardfault_args) {
//	unsigned int stacked_r0;
//	unsigned int stacked_r1;
//	unsigned int stacked_r2;
//	unsigned int stacked_r3;
//	unsigned int stacked_r12;
//	unsigned int stacked_lr;
//	unsigned int stacked_pc;
//	unsigned int stacked_psr;
//
//	stacked_r0 = ((unsigned long) hardfault_args[0]);
//	stacked_r1 = ((unsigned long) hardfault_args[1]);
//	stacked_r2 = ((unsigned long) hardfault_args[2]);
//	stacked_r3 = ((unsigned long) hardfault_args[3]);
//
//	stacked_r12 = ((unsigned long) hardfault_args[4]);
//	stacked_lr = ((unsigned long) hardfault_args[5]);
//	stacked_pc = ((unsigned long) hardfault_args[6]);
//	stacked_psr = ((unsigned long) hardfault_args[7]);
//
//	printf("\n\n[Hard fault handler - all numbers in hex]\n");
//	printf("R0 = %x\n", stacked_r0);
//	printf("R1 = %x\n", stacked_r1);
//	printf("R2 = %x\n", stacked_r2);
//	printf("R3 = %x\n", stacked_r3);
//	printf("R12 = %x\n", stacked_r12);
//	printf("LR [R14] = %x  subroutine call return address\n", stacked_lr);
//	printf("PC [R15] = %x  program counter\n", stacked_pc);
//	printf("PSR = %x\n", stacked_psr);
//	printf("BFAR = %x\n", (*((volatile unsigned long *) (0xE000ED38))));
//	printf("CFSR = %x\n", (*((volatile unsigned long *) (0xE000ED28))));
//	printf("HFSR = %x\n", (*((volatile unsigned long *) (0xE000ED2C))));
//	printf("DFSR = %x\n", (*((volatile unsigned long *) (0xE000ED30))));
//	printf("AFSR = %x\n", (*((volatile unsigned long *) (0xE000ED3C))));
//	printf("SCB_SHCSR = %x\n", SCB ->SHCSR);
//
//	if (SCB->HFSR & SCB_HFSR_DEBUGEVT_Msk) {
//		printf(" This is a DEBUG FAULT\n");
//	} else if (SCB ->HFSR & SCB_HFSR_FORCED_Msk) {
//		printf(" This is a FORCED FAULT\n");
//
//		if (SCB ->CFSR & (0x1 << SCB_CFSR_USGFAULTSR_Msk)) {
//			printf("undefined instruction\n");
//		} else if (SCB ->CFSR & (0x2 << SCB_CFSR_USGFAULTSR_Pos)) {
//			printf("instruction makes illegal use of the EPSR\n");
//		} else if (SCB ->CFSR & (0x4 << SCB_CFSR_USGFAULTSR_Pos)) {
//			printf("Invalid PC load UsageFault, caused by an invalid PC load by EXC_RETURN\n");
//		} else if (SCB ->CFSR & (0x8 << SCB_CFSR_USGFAULTSR_Pos)) {
//			printf("The processor does not support coprocessor instructions\n");
//		} else if (SCB ->CFSR & (0x100 << SCB_CFSR_USGFAULTSR_Pos)) {
//			printf("Unaligned access\n");
//		} else if (SCB ->CFSR & (0x200 << SCB_CFSR_USGFAULTSR_Pos)) {
//			printf("Divide by zero\n");
//		}
//	} else if (SCB ->HFSR & SCB_HFSR_VECTTBL_Pos) {
//		printf(" This is a BUS FAULT\n");
//	}
	while (1) {
		;
	}
}
