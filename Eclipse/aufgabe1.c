#include "aufgabe1.h"

//========== Macros

//========== Variablen
char usart2_rx_buffer[50];
char usart2_tx_buffer[50];

//========== Funktionen

void init_leds(void) {
	// Struct anlegen
	GPIO_InitTypeDef GPIO_InitStructure;

	// Struct Initialisieren
	// setzt alle Leitungen auf Eingang ohne PushPull
	GPIO_StructInit(&GPIO_InitStructure);

	// Funktion der Portleitungen festlegen
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_14 | GPIO_Pin_15; //Portleitungen 8, 14, 15
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; //GPIO Output Mode
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz; //Low speed
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD; //Open Drain
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL; //No Pull

	// Portleitungen initialisieren
	// Werte aus dem Struct werden in die MCU-Register geschrieben
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	// LEDs an
	// GPIO_ResetBits(GPIOB, GPIO_Pin_8 | GPIO_Pin_14 | GPIO_Pin_15);

	// LEDs aus
	GPIO_SetBits(GPIOB, GPIO_Pin_8 | GPIO_Pin_14 | GPIO_Pin_15);
}

void wait_sec(unsigned long s) {
	wait_uSek(1000000 * s);
}

void aufgabe1_1_4() {
	while (1) {
		RT_LED_TOGGLE;
		wait_sec(1);
	}
}

void aufgabe1_1_5() {
	while (1) {
		AMPEL_RT;
		wait_sec(1);
		AMPEL_RT_GB;
		wait_sec(1);
		AMPEL_GR;
		wait_sec(1);
		AMPEL_GB;
		wait_sec(1);
	}
}

void aufgabe1_1_6() {
	while (1) {
		wait_sec(2);
		RT_LED_ON;
		wait_sec(2);
		RT_LED_OFF;
		wait_sec(2);
		GB_LED_ON;
		wait_sec(2);
		GB_LED_OFF;
		wait_sec(2);
		GR_LED_ON;
		wait_sec(2);
		GR_LED_OFF;
	}
}

void init_taste_1(void) {
	// Struct anlegen
	GPIO_InitTypeDef GPIO_InitStructure;

	// Struct Initialisieren
	// setzt alle Leitungen auf Eingang ohne PushPull
	GPIO_StructInit(&GPIO_InitStructure);

	// Funktion der Portleitungen festlegen
	//Portleitung 13
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	//GPIO Output Mode
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN; 
	//Medium speed
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
	//PushPull
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	//No Pull
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	// Portleitungen initialisieren
	// Werte aus dem Struct werden in die MCU-Register geschrieben
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void init_taste_2(void) {
	// Struct anlegen
	GPIO_InitTypeDef GPIO_InitStructure;

	// Struct Initialisieren
	// setzt alle Leitungen auf Eingang ohne PushPull
	GPIO_StructInit(&GPIO_InitStructure);

	// Funktion der Portleitungen festlegen
	//Portleitung 0
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	//GPIO Output Mode
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	//Medium speed
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz; 
	//PushPull
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	//No Pull
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL; 

	// Portleitungen initialisieren
	// Werte aus dem Struct werden in die MCU-Register geschrieben
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void aufgabe1_2_3() {
	init_taste_1();
	init_taste_2();
	while(1) {
		uint8_t pressed1 = read_taste_1();
		uint8_t pressed2 = read_taste_2();
		// Wenn Taste 1 gedrückt: rot aus, grün an
		if (pressed1) {
			GR_LED_ON;
			RT_LED_OFF;
		}
		// Wenn Taste 2 gedrückt: rot an, grün aus
		if (pressed2) {
			GR_LED_OFF;
			RT_LED_ON;
		}

		// Wenn irgendeine Taste gedrückt: gelb an
		// sonst: gelb aus
		if (pressed1 | pressed2) {
			GB_LED_ON;
		} else {
			GB_LED_OFF;
		}
	}
}

void aufgabe1_2_4() {
	init_taste_1();
	init_taste_2();
	int count1Pressed = 0;
	int count2Pressed = 0;
	int button1Down = 0;
	int button2Down = 0;
	int redLedOn = 0;
	while (1) {
		uint8_t pressed1 = read_taste_1();
		uint8_t pressed2 = read_taste_2();

		// Taste 1 war nicht gedrückt, aber jetzt => Tastendruck startet
		if (!button1Down && pressed1)
			button1Down = 1;
		// Taste 2 war nicht gedrückt, aber jetzt => Tastendruck startet
		if (!button2Down && pressed2)
			button2Down = 1;

		// Taste 1 war gedrückt und jetzt nicht mehr => Tastendruck abgeschlossen
		if (button1Down && !pressed1) {
			button1Down = 0;
			count1Pressed++;
		}
		// Taste 2 war gedrückt und jetzt nicht mehr => Tastendruck abgeschlossen
		if (button2Down && !pressed2) {
			button2Down = 0;
			count2Pressed++;
			// Rote LED nicht an und Taste 2 gedrückt => gelbe LED umschalten
			if (!redLedOn)
				GB_LED_TOGGLE;
		}

		// Taste 1 wurde 10x gedrückt => rote LED an
		if (!redLedOn && count1Pressed == 10) {
			redLedOn = 1;
			RT_LED_ON;
			count1Pressed = 0;
			count2Pressed = 0;
		// Taste 2 wurde 3x gedrückt => rote LED aus
		} else if (redLedOn && count2Pressed == 3) {
			redLedOn = 0;
			RT_LED_OFF;
			count1Pressed = 0;
			count2Pressed = 0;
		}

		// Grüne LED an/aus entspricht Druckzustand von Taste 1
		if (pressed1)
			GR_LED_ON;
		else
			GR_LED_OFF;
	}
}

uint8_t read_taste_1() {
	return GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_13);
}

uint8_t read_taste_2() {
	return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0);
}