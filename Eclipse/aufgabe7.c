#include "aufgabe7.h"

void aufgabe7(int part) {
	aufgabe_7_aktiv = part;

	GPIO_DeInit(GPIOA);
	GPIO_DeInit(GPIOB);

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	// Tasten initialisieren
	init_taste_1();

	if (part < 3)
		init_taste_2();

	// USART initialisieren
	if (part < 3)
		init_usart_2_tx();

	// LEDs initialisieren
	init_leds();

	// Interrupt auf Taste 2 aktivieren
	if (part < 3)
		init_taste_2_irq(EXTI_Trigger_Rising);

	int counter = 0;
	if (part < 3)
		usart_2_print("System Neustart\r\n");
	while (1) {
		if (read_taste_1()) {
			GB_LED_ON;
			if (part == 1) {
				usart_2_print("Sleep Mode Start\r\n");
				__WFI();
				usart_2_print("Sleep Mode Ende\r\n");
			} else if (part == 2) {
				usart_2_print("Stop Mode Start\r\n");
				PWR_FlashPowerDownCmd(ENABLE);
				PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
				PWR_FlashPowerDownCmd(DISABLE);
				SystemInit();
				usart_2_print("Stop Mode Ende\r\n");
			} else if (part == 3) {
				PWR_WakeUpPinCmd(ENABLE);
				wait_sec(5);
				PWR_EnterSTANDBYMode();
			} else if (part == 4) {
				// Disable RTC wakeup interrupt
				RTC_ITConfig(RTC_IT_WUT, DISABLE);
				// Clear RTC Wakeup WUTF Flag
				RTC_ClearITPendingBit(RTC_IT_WUT);
				RTC_ClearFlag(RTC_FLAG_WUTF);

				// Set Wakeup Alarm
				RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
				init_alarm_interrupt();
				time_alarm(40);

				// Clear PWR Wakeup WUF Flag
				PWR_ClearFlag(PWR_CSR_WUF);
				// Enable RTC Wakeup Interrupt
				RTC_ITConfig(RTC_IT_WUT, ENABLE);
				PWR_WakeUpPinCmd(ENABLE);
				wait_sec(5);
				// Enter Standby mode
				PWR_EnterSTANDBYMode();
			}
		}

		wait_uSek(10000);
		counter++;
		if (counter == 300) {
			counter = 0;
			GR_LED_TOGGLE;
		}
	}
}
