#ifndef __AUFGABE12_H__
#define __AUFGABE12_H__

//=========================================================================
// cmsis_lib
//=========================================================================
#include "stm32f4xx.h"
//#include "misc.h"
//#include "stm32f4xx_adc.h"
//#include "stm32f4xx_can.h"
//#include "stm32f4xx_crc.h"
//#include "stm32f4xx_cryp_aes.h"
//#include "stm32f4xx_cryp_des.h"
//#include "stm32f4xx_cryp_tdes.h"
//#include "stm32f4xx_cryp.h"
//#include "stm32f4xx_dac.h"
//#include "stm32f4xx_dbgmcu.h"
//#include "stm32f4xx_dcmi.h"
//#include "stm32f4xx_dma.h"
//#include "stm32f4xx_exti.h"
//#include "stm32f4xx_flash.h"
//#include "stm32f4xx_fsmc.h"
#include "stm32f4xx_gpio.h"
//#include "stm32f4xx_hash_md5.h"
//#include "stm32f4xx_hash_sha1.h"
//#include "stm32f4xx_hash.h"
//#include "stm32f4xx_i2c.h"
//#include "stm32f4xx_iwdg.h"
//#include "stm32f4xx_pwr.h"
//#include "stm32f4xx_rcc.h"
//#include "stm32f4xx_rng.h"
//#include "stm32f4xx_rtc.h"
//#include "stm32f4xx_sdio.h"
//#include "stm32f4xx_spi.h"
//#include "stm32f4xx_syscfg.h"
//#include "stm32f4xx_tim.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_wwdg.h"

//=========================================================================
// board_lib
//=========================================================================

#include "aufgabe1.h"
#include "aufgabe7.h"
#include "aufgabe9.h"

//=========================================================================
// standart_lib
//=========================================================================
#include "spi.h"
//#include "stdio.h"
//#include "string.h"

//=========================================================================
// CooCox CoOS
//=========================================================================
//#include "CoOS.h"
#include "CC3000.h"
//=========================================================================
// Eigene Funktionen, Macros und Variablen
//=========================================================================
//========== Variablen

int aufgabe_12_aktiv;

int next_send_command;

int state_red_other;
int state_green_other;
int state_red_local;
int state_green_local;

int source_id, target_id;



int color_toggle;
int set_ntp_time;

unsigned long echo_socket_handle;
int send_buffer;

//========== Macros

#define MASK_COL 0x01
#define MASK_CMD 0x02
#define MASK_CONFIRM 0xF0
#define RED 0x00
#define GREEN 0x01
#define ON 0x02
#define OFF 0x00
#define CONFIRM 0x10

//========== Funktionen

void aufgabe12_1_1();
void aufgabe12_1_2();
void aufgabe12_1_3(int sid, int tid);
void aufgabe12_2_1();
void aufgabe12_2_2();
void aufgabe12_2_3();
void aufgabe12_2_4();
void receive_data_callback(char* data, unsigned int length);

void taster_callback(int taster);

#endif
