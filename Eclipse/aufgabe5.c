#include "aufgabe5.h"

void aufgabe5_base() {
	// GPIO initialisieren
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	GPIO_DeInit(GPIOA);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	// LEDs initialisieren
	init_leds();

	// Tasten initialisieren
	init_taste_1();
	init_taste_2();
}

void aufgabe5_1_1() {
	aufgabe5_base();

	aufgabe_5_aktiv = 1;

	// Interrupts initialisieren
	init_taste_1_irq(EXTI_Trigger_Rising);
	init_taste_2_irq(EXTI_Trigger_Rising);

	while (1) {
	}
}

void aufgabe5_1_2() {
	aufgabe5_base();

	aufgabe_5_aktiv = 2;

	// Interrupts initialisieren
	init_taste_1_irq(EXTI_Trigger_Rising);
	init_taste_2_irq_inv();

	while (1) {
	}
}

void aufgabe5_1_3() {
	aufgabe5_base();

	aufgabe_5_aktiv = 3;

	// USART initialisieren
	init_usart_2_tx();

	// Interrupts initialisieren
	init_taste_1_irq(EXTI_Trigger_Rising);
	init_taste_2_irq(EXTI_Trigger_Rising);

	while (1) {
	}
}

void aufgabe5_2_1() {
	aufgabe_5_aktiv = 4;
	// LEDs initialisieren
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_leds();

	// USART initialisieren
	init_usart_2();

	// RXNE interrupt erlauben
	// Erlaubt Interrupt wenn Zeichen Empfangen
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	// USART6 Interrupt global erlauben
	NVIC_EnableIRQ(USART2_IRQn);

	while (1) {
	}
}

void aufgabe5_2_2() {
	aufgabe_5_aktiv = 5;

	// LEDs initialisieren
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_leds();

	// USART initialisieren
	init_usart_2();

	// RXNE interrupt erlauben
	// Erlaubt Interrupt wenn Zeichen Empfangen
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	// USART6 Interrupt global erlauben
	NVIC_EnableIRQ(USART2_IRQn);

	while (1) {
	}
}

void aufgabe5_2_3() {
	unsigned char value_watchdog_counter = 0x7f;
	unsigned char window_value = 0x50;
	unsigned char window_value_refresh = 0x39;
	unsigned char cnt_i = 0;
	unsigned char cnt_j = 0;

	init_usart_2();

	sprintf(usart2_tx_buffer, "\r\nNeustart\r\n");
	usart_2_print(usart2_tx_buffer);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);

	WWDG_SetPrescaler(WWDG_Prescaler_8);
	WWDG_SetWindowValue(window_value);
	WWDG_Enable(value_watchdog_counter);

	// Interruptflag zurücksetzen
	WWDG_ClearFlag();

	// Interrupcontroller konfigurieren
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = WWDG_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Interrupt freigeben
	WWDG_EnableIT();

	cnt_i = (unsigned char) (value_watchdog_counter + 1);

	while (1) {

		cnt_j = (unsigned char) ((WWDG->CR) & 0x7F);

		if (cnt_j < cnt_i) {

			sprintf(usart2_tx_buffer, "i = %u\r\n", cnt_j);
			usart_2_print(usart2_tx_buffer);

			cnt_i = cnt_j;

			if (cnt_i == window_value_refresh) {

				WWDG_SetCounter(value_watchdog_counter);

				sprintf(usart2_tx_buffer, "####### neu geladen\r\n");
				usart_2_print(usart2_tx_buffer);

				cnt_i = (unsigned char) (value_watchdog_counter + 1);
			}
		}
	}
}

void set_nvic_state(uint8_t channel, FunctionalState state) {
	// Anlegen eines NVIC Struct
	NVIC_InitTypeDef NVIC_InitStructure;

	// Festlegung der Interruptquelle
	NVIC_InitStructure.NVIC_IRQChannel = channel;

	// Niedrige Priorität setzen
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	// Interruptkanal Freigabe/Sperren
	NVIC_InitStructure.NVIC_IRQChannelCmd = state;

	// Register aus dem Struct heraus schreiben
	NVIC_Init(&NVIC_InitStructure);
}

void set_exti_line(uint32_t exti_line, EXTITrigger_TypeDef trigger,
		FunctionalState state) {
	// Struct anlegen
	EXTI_InitTypeDef EXTI_InitStructure;

	// EXTI_Line zweisen
	EXTI_InitStructure.EXTI_Line = exti_line;

	// Interrupt Mode setzen
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;

	// Triggerbedingung setzen
	EXTI_InitStructure.EXTI_Trigger = trigger;

	// Interrupt erlauben
	EXTI_InitStructure.EXTI_LineCmd = state;

	// Register aus dem Struct heraus setzen
	EXTI_Init(&EXTI_InitStructure);
}

void init_taste_1_irq(EXTITrigger_TypeDef trigger) {
	// Bindet Port B Leitung 13 an die EXTI_Line13 Leitung
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource13);

	enable_taste1_irq(trigger);

	set_nvic_state(EXTI15_10_IRQn, ENABLE);
}

void init_taste_2_irq(EXTITrigger_TypeDef trigger) {
	// Bindet Port A Leitung 0 an die EXTI_Line0 Leitung
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);

	enable_taste2_irq(trigger);

	set_nvic_state(EXTI0_IRQn, ENABLE);
}

void init_taste_2_irq_inv() {
	// Bindet Port A Leitung 0 an die EXTI_Line0 Leitung
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);

	set_exti_line(EXTI_Line0, EXTI_Trigger_Falling, ENABLE);

	set_nvic_state(EXTI0_IRQn, ENABLE);
}

void enable_taste1_irq(EXTITrigger_TypeDef trigger) {
	set_exti_line(EXTI_Line13, trigger, ENABLE);
}

void enable_taste2_irq(EXTITrigger_TypeDef trigger) {
	set_exti_line(EXTI_Line0, trigger, ENABLE);
}

void disable_taste1_irq() {
	set_exti_line(EXTI_Line13, EXTI_Trigger_Rising, DISABLE);
}

void disable_taste2_irq() {
	set_exti_line(EXTI_Line0, EXTI_Trigger_Rising, DISABLE);
}

