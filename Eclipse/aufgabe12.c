#include "aufgabe12.h"

//set_CHANNEL(8);
//// Setzen der eigenen Funk-ID (0..255)
//set_ID(108);

//while(1){
//wait 1sek

//send_Packet(8,108,"hallo",5);
//}

void init_ism(unsigned char channel, unsigned char id) {
	// Initialisierung des CC1101
	init_CC1101_SPI1(); // SPI Initialisierung
	init_CC1101(); // CC1101 Konfigurieren
	init_CC1101_IRQ5(); // Interrupt Initialisierung
	// Einstellen des Funkkanals (1..10)
	set_CHANNEL(channel);
	// Setzen der eigenen Funk-ID (0..255)
	set_ID(id);
}

void print_packet_string(char* data, unsigned int length) {
	unsigned int i;
	char string_data[length + 1];
	for (i = 0; i < length; i++) {
		string_data[i] = data[i];
	}
	string_data[i] = 0;
	usart_2_print(string_data);
}

void receive_data_callback(char* data, unsigned int length) {
	if (aufgabe_12_aktiv == 1) {
		print_packet_string(data, length);
	} else if (aufgabe_12_aktiv == 3) {
		if (length != 1)
			return;
		char msg = data[0];
		int cmd = msg & MASK_CMD;
		int color = msg & MASK_COL;
		int confirm = msg & MASK_CONFIRM;
		if (confirm) {
			if (color == RED)
				state_red_other = cmd;
			else if (color == GREEN)
				state_green_other = cmd;
		} else {
			if (color == RED) {
				if (cmd == ON)
					RT_LED_ON;
				else
					RT_LED_OFF;
			} else if (color == GREEN) {
				if (cmd == ON)
					GR_LED_ON;
				else
					GR_LED_OFF;
			}
			char response[1] = { CONFIRM | color | cmd };
			send_Packet(target_id, source_id, response, 1);
		}
	}
}

void aufgabe12_1_1() {
	aufgabe_12_aktiv = 1;
	init_usart_2();
	init_ism(8, 0);
	while (1) {
	}
}

void send_string(char* data, unsigned char source, unsigned char target) {
	unsigned int MAX_SEND_LENGTH = 60 - 1;
	unsigned int sent_bytes = 0;
	while (sent_bytes < strlen(data)) {
		char* data_with_offset = data + sent_bytes;
		int length =
				(strlen(data_with_offset) > MAX_SEND_LENGTH) ?
						MAX_SEND_LENGTH : strlen(data_with_offset);
		send_Packet(target, source, data_with_offset, length);
		sent_bytes += length;
		wait_uSek(1000000);
	}
}

void aufgabe12_1_2() {
	wait_uSek(5000000);

	// Disable RTC wakeup interrupt
	RTC_ITConfig(RTC_IT_WUT, DISABLE);
	// Clear RTC Wakeup WUTF Flag
	RTC_ClearITPendingBit(RTC_IT_WUT);
	RTC_ClearFlag(RTC_FLAG_WUTF);

	// Init Taste1
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_taste_1();

	// Tastenzustand abfragen
	uint8_t taste1_pressed = read_taste_1();

	// Batteriespannung messen
	init_measuring(TYPE_BATTERY_VOLTAGE);
	float battery_voltage = get_measure_value(TYPE_BATTERY_VOLTAGE);

	// Temperatur messen
	init_measuring(TYPE_TEMPERATURE);
	float temperature = get_measure_value(TYPE_TEMPERATURE);

	// Zeitstempel
	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	RTC_GetDate(RTC_Format_BIN, &date);
	RTC_GetTime(RTC_Format_BIN, &time);

	init_ism(8, 1);
	char buffer[255];
	char* taste1_state = (taste1_pressed) ? "pressed" : "not pressed";
	sprintf(buffer,
			"%02i.%02i.%02i %02i:%02i:%02i - Voltage: %f V, Temperature: %f °C, Taste 1: %s\r\n",
			date.RTC_Date, date.RTC_Month, date.RTC_Year, time.RTC_Hours,
			time.RTC_Minutes, time.RTC_Seconds, battery_voltage, temperature,
			taste1_state);

	// Senden des Status-Strings
	send_string(buffer, 1, 0);

	// Set Wakeup Alarm
	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
	init_alarm_interrupt();
	time_alarm(30);

	// Clear PWR Wakeup WUF Flag
	PWR_ClearFlag(PWR_CSR_WUF);
	// Enable RTC Wakeup Interrupt
	RTC_ITConfig(RTC_IT_WUT, ENABLE);
	PWR_WakeUpPinCmd(ENABLE);

	// Enter Standby mode
	PWR_EnterSTANDBYMode();
}

void aufgabe12_1_3(int sid, int tid) {
	aufgabe_12_aktiv = 3;

	source_id = sid;
	target_id = tid;

	state_green_local = 0;
	state_green_other = 0;
	state_red_local = 0;
	state_red_other = 0;

	GPIO_DeInit(GPIOA);
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	init_leds();

	init_taste_1();
	init_taste_2();
	init_taste_1_irq(EXTI_Trigger_Rising_Falling);
	init_taste_2_irq(EXTI_Trigger_Rising_Falling);

	init_ism(8, source_id);

	char msg[1];

	while (1) {
		if (state_green_local != state_green_other) {
			msg[0] = GREEN | state_green_local;
			send_Packet(target_id, source_id, msg, 1);
		} else if (state_red_local != state_red_other) {
			msg[0] = RED | state_red_local;
			send_Packet(target_id, source_id, msg, 1);
		}
		wait_uSek(300000 + (random() % 300000));
	}

}

void taster_callback(int taster) {
	if (taster == 1)
		state_red_local ^= ON;
	else
		state_green_local ^= ON;
}

void aufgabe12_2_1() {
	init_usart_2();

	// Übersetzung der SecurityTypes
	char secTypes[4][6] = { "Offen", "WEP", "WPA", "WPA2" };

	// WLAN initialisieren
	usart_2_print("Init WLAN\r\n");
	CC3000_Init();
	usart_2_print("Init Complete\r\n");

	char buffer[256];
	unsigned long i;

	while (1) {
		// Nach WLAN-Netzen suchen
		unsigned long count = CC3000_ScanNetworks(visibleNetworks);
		sprintf(buffer, "Found %lu networks:\r\n", count);
		usart_2_print(buffer);

		// Gefundene Netzwerke auf USART ausgeben
		for (i = 0; i < count; i++) {
			network_t* net = &visibleNetworks[i];
			sprintf(buffer, "%lu: %s, Signalstärke: %u, Sicherheit: %s\r\n",
					i + 1, net->strSSID, net->uiRSSI, secTypes[net->uiSecMode]);
			usart_2_print(buffer);
		}
		usart_2_read();
	}
}

void try_connect() {
	unsigned char key[] = { '0', '8', '1', '5', 'a', 'c', 'h', 'i' }; // 0815achi
	unsigned int key_length = 8;

	while (!CC3000_is_Connected) {
		// Nach Access Points suchen
		unsigned long count = CC3000_ScanNetworks(visibleNetworks);
		int i;
		for (i = 0; i < count; i++) {
			network_t* net = &visibleNetworks[i];
			if (strcmp("MPP_IoT", net->strSSID) == 0) {
				long result = wlan_connect(net->uiSecMode, net->strSSID,
						strlen(net->strSSID), NULL, key, key_length);
				// Verbinden erfolgreich
				if (result == 0) {
					int wait = 0;
					// Warte auf Verbindung
					while (!CC3000_is_Connected) {
						if (wait++ > 10)
							break;
						wait_uSek(500000);
					}
					// Signalisierung: Verbindung erfolgreich
					init_pwm_signal_timer(NOTE_C);
					wait_uSek(250000);
					init_pwm_signal_timer(NOTE_G);
					wait_uSek(250000);
					disable_pwm_timer();
					GR_LED_ON;
				}
				break;

			}
		}
	}
}

void aufgabe12_2_2() {
	aufgabe_12_aktiv = 4;

	init_usart_2();
	CC3000_Init();
	init_pwm();
	init_leds();

	while (1) {
		// Gelb blinken
		color_toggle = 1;
		init_tim7_interrupt(4);

		// 60 Sekunden countdown bis Standby
		RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
		init_alarm_interrupt();
		time_alarm(60);

		try_connect();

		// Standby countdown aus, Gelb blinkt nicht mehr
		RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
		TIM_Cmd(TIM7, DISABLE);
		GB_LED_OFF;

		// Warte auf disconnect
		while (CC3000_is_Connected) {
		}

		// Grüne LED aus
		GR_LED_OFF;

		// Rote LED blinken lassen
		color_toggle = 2;
		init_tim7_interrupt(4);
		while (1) {
		}
	}
}

void aufgabe12_2_3() {
	const const char* WEEKDAYS[] = { "Montag", "Dienstag", "Mittwoch",
			"Donnerstag", "Freitag", "Samstag", "Sonntag" };

	aufgabe_12_aktiv = 5;

	init_usart_2();
	CC3000_Init();
	init_pwm();
	init_leds();

	while (1) {
		// Gelb blinken
		color_toggle = 1;
		init_tim7_interrupt(4);

		// 60 Sekunden countdown bis Standby
		RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
		init_alarm_interrupt();
		time_alarm(60);

		try_connect();

		// Standby countdown aus, Gelb blinkt nicht mehr
		RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
		TIM_Cmd(TIM7, DISABLE);
		GB_LED_OFF;

		RTC_DateTypeDef date;
		RTC_TimeTypeDef time;
		char buffer[50];

		// Interrupt für USART aktivieren
		USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
		NVIC_EnableIRQ(USART2_IRQn);

		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
		init_taste_2();
		init_taste_2_irq(EXTI_Trigger_Rising);

		// Wait for internet connection
		while (!CC3000_DHCP_done) {
		}

		// Warte auf disconnect
		while (CC3000_is_Connected) {
			if (!setting_time && set_ntp_time) {
				set_ntp_time = false;
				get_NTP_Time(NULL);
			}

			if (!setting_time) {
				RTC_GetDate(RTC_Format_BIN, &date);
				RTC_GetTime(RTC_Format_BIN, &time);
				sprintf(buffer,
						"Heute ist %s der %02i.%02i.%02i. Es ist %02i:%02i:%02i Uhr.\r\n",
						WEEKDAYS[date.RTC_WeekDay - 1], date.RTC_Date,
						date.RTC_Month, date.RTC_Year, time.RTC_Hours,
						time.RTC_Minutes, time.RTC_Seconds);
				usart_2_print(buffer);
			}

			wait_sec(5);
		}

		// Grüne LED aus
		GR_LED_OFF;

		// Rote LED blinken lassen
		color_toggle = 2;
		init_tim7_interrupt(4);
		while (1) {
		}
	}
}

void echo_read_callback(char* rx, uint16_t rx_len, sockaddr* from,
		uint16_t socket_Position) {
	usart_2_print(rx);
	usart_2_print("\r\n");
	if (strcmp(rx, "green on\r") == 0) {
		GR_LED_ON;
	}
	if (strcmp(rx, "green off\r") == 0) {
		GR_LED_OFF;
	}
	if (strcmp(rx, "yellow on\r") == 0) {
		GB_LED_ON;
	}
	if (strcmp(rx, "yellow off\r") == 0) {
		GB_LED_OFF;
	}
	if (strcmp(rx, "red on\r") == 0) {
		RT_LED_ON;
	}
	if (strcmp(rx, "red off\r") == 0) {
		RT_LED_OFF;
	}

	memset(&rx_daten, 0, sizeof(rx_daten));
}

long echo_write_callback(char* tx, uint16_t tx_len, uint16_t socket_Position) {
	if (send_buffer) {
		send_buffer = 0;
		strcpy(tx, usart2_rx_buffer);
		return strlen(tx);
	}
	return 0;
}

void aufgabe12_2_4() {
	aufgabe_12_aktiv = 6;
	init_usart_2();
	CC3000_Init();
	init_pwm();
	init_leds();

	// Gelb blinken
	color_toggle = 1;
	init_tim7_interrupt(4);

	try_connect();

	// Standby countdown aus, Gelb blinkt nicht mehr
	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
	TIM_Cmd(TIM7, DISABLE);
	GB_LED_OFF;
	unsigned long echo_server = 0L;
	char server_name[] = "eel.mi.fu-berlin.de";
	uint16_t echo_port = 50008;

	// Warte auf Verbindungsaufbau
	while (!CC3000_DHCP_done) {
	}

	// Öffne Socket zum Echo Server
	gethostbyname(server_name, strlen(server_name), &echo_server);
	echo_socket_handle = CC3000_openSocketul(echo_server, echo_port, true,
			false, false, true, echo_read_callback, echo_write_callback, 0);

	// Interrupt für USART aktivieren
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	NVIC_EnableIRQ(USART2_IRQn);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	init_taste_1();
	init_taste_1_irq(EXTI_Trigger_Rising);
	init_taste_2();
	init_taste_2_irq(EXTI_Trigger_Rising);

	while (1) {
	}
}

