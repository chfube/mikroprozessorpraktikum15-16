#include "aufgabe2.h"
//========== Macros

//========== Variablen

//========== Funktionen

void RCC_WaitForPLLStartUp(void) {
	while ((RCC->CR & RCC_CR_PLLRDY) == 0) {
		__NOP();
	}
}

//==== Taktfrequenz 24MHz mit HSE-OSC=16MHz
void slowMode(void) {
	RCC_DeInit();

	RCC_HSEConfig(RCC_HSE_ON);
	if (RCC_WaitForHSEStartUp() == ERROR) {
		return;
	}
	// HSEOSC=16MHz SYSCLK=24MHz HCLK=24MHz
	// PCLK1=24 PCLK2=24MHz
	RCC_PLLConfig(RCC_PLLSource_HSE, //RCC_PLLSource
			16, //PLLM
			192, //PLLN
			8, //PLLP
			4 //PLLQ
			);
	RCC_PLLCmd(ENABLE);
	RCC_WaitForPLLStartUp();

	// Configures the AHB clock (HCLK)
	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	// Low Speed APB clock (PCLK1)
	RCC_PCLK1Config(RCC_HCLK_Div1);
	// High Speed APB clock (PCLK2)
	RCC_PCLK2Config(RCC_HCLK_Div1);

	// select system clock source
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
}

//==== Taktfrequenz 168MHz mit HSE-OSC=16MHz
void fastMode(void) {
	RCC_DeInit();

	RCC_HSEConfig(RCC_HSE_ON);
	if (RCC_WaitForHSEStartUp() == ERROR) {
		return;
	}
	// HSEOSC=16MHz SYSCLK=168MHz HCLK=168MHz
	// PCLK1=42MHz PCLK2=84MHz
	RCC_PLLConfig(RCC_PLLSource_HSE, //RCC_PLLSource
			16, //PLLM
			336, //PLLN
			2, //PLLP
			7 //PLLQ
			);
	RCC_PLLCmd(ENABLE);
	RCC_WaitForPLLStartUp();

	// Configures the AHB clock (HCLK)
	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	// High Speed APB clock (PCLK1)
	RCC_PCLK1Config(RCC_HCLK_Div4);
	// High Speed APB clock (PCLK2)
	RCC_PCLK2Config(RCC_HCLK_Div2);

	// select system clock source
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
}

void init_PC09() {
	// Clocksystem für die Peripherie aktivieren
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	// Struct anlegen
	GPIO_InitTypeDef GPIO_InitStructure;
	// Output PIN 9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	// Alternativer Modus
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	// PushPull
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	// PullUp
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	// Fast speed
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	// Struct auf Port C initialisieren
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	// Verbindet microcontroller clock output (MCO) mit dem alternativen Output
	GPIO_PinAFConfig(GPIOC, GPIO_Pin_9, GPIO_AF_MCO);
	// Output für SysClock, ohne Prescaling
	RCC_MCO2Config(RCC_MCO2Source_SYSCLK, RCC_MCO2Div_1);
}

void aufgabe2_1_2() {
	// Reset LED Ports
	GPIO_DeInit(GPIOB);
	// Clocksystem für die Peripherie aktivieren
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	// Reset Clock Output Port
	GPIO_DeInit(GPIOC);
	init_leds();
	init_PC09();

	while (1) {
		RT_LED_TOGGLE;
		wait_sec(1);
	}
}

void aufgabe2_1_4() {
	SystemInit();
	// Beim 2. Teil der Aufgabe wird dies nicht ausgeführt
	//GPIO_DeInit(GPIOC);
	//init_PC09();
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_taste_1();
	GPIO_DeInit(GPIOA);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	init_taste_2();
	while (1) {
		uint8_t pressed1 = read_taste_1();
		uint8_t pressed2 = read_taste_2();
		if (pressed1)
			slowMode();
		if (pressed2)
			fastMode();
	}
}

void aufgabe2_2_1() {
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_leds();
	while (1) {
	}
}

void aufgabe2_2_2() {
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_leds();
	while (1) {
		GR_LED_ON;
		timer = 10;
		while (timer) {
		} // wartet 1 Sekunde
		GR_LED_OFF;
		timer = 100;
		while (timer) {
		} // wartet 10 Sekunden
	}
}
