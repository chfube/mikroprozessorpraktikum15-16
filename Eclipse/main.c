#include "main.h"

int main(void) {

	// Initialisierung des Systems und des Clocksystems
	SystemInit();

	//	SysTick initialisieren
	//	alle 10ms erfolgt dann der Aufruf
	//	des Handlers fuer den Interrupt SysTick_IRQn
	InitSysTick();

	//	Start der RTC  falls diese noch
	//	nicht initialisiert war wird
	//	die RTC mit der LSE-Taktquelle aktiviert
	start_RTC();

	//	Initialisierung aller Portleitungen und Schnittstellen
	//	Freigabe von Interrupten
	init_board();

	//	Bei Nutzung des CoOS einbinden
	//	Initialisierung des CoCox Real Time Betriebssystems CoOS
	//CoInitOS ();
	//	Anmelden notwendiger applikationsspezifischer Task
	//CoCreateTask (...);
	//	Start des Betriebssystems CoOS
	//CoStartOS ();

	aufgabe8_2_1();

	return 0;
}
