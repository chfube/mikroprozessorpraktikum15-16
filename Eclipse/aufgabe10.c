#include "aufgabe10.h"

void init_dac() {
	// Taktsystem einschalten
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// GPIO Konfigurieren
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// DAC_OUT1 Kanal Konfigurieren
	DAC_InitTypeDef DAC_InitStructure;
	DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;
	DAC_Init(DAC_Channel_1, &DAC_InitStructure);

	// DAC_OUT1 Kanal Freigeben
	DAC_Cmd(DAC_Channel_1, ENABLE);
}

// Berechnet den DAU-Wert für die gegebene Spannung
unsigned int get_dau_value(float voltage, unsigned int dau_value_bit_range) {
	unsigned int dau_value_range = (1 << dau_value_bit_range) - 1;
	return voltage * dau_value_range / 3.3f;
}

// Setzt die Ausgabespannung auf den übergebenen Wert
void set_output_voltage(float voltage, unsigned int dau_value_bit_range) {
	unsigned int dau_value = get_dau_value(voltage, dau_value_bit_range);

	if (dau_value_bit_range == 12)
		DAC_SetChannel1Data(DAC_Align_12b_R, dau_value);
	else if (dau_value_bit_range == 8)
		DAC_SetChannel1Data(DAC_Align_8b_R, dau_value);
}

// Berechnet eine volle Sinusperiode und speichert diese 
// in einem Array mit 100 Einträgen.
void init_sinus_output() {
	int x;
	for (x = 0; x < 100; x++) {
		sinus_voltage[x] = sin(M_PI * x / 50) + 1.5f;
	}
}

// Aktiviert den Timer-Interrupt, in dem die 
// Ausgabespannung gesetzt wird.
void start_sinus_output(int hz) {
	sinus_x_pos = 0;
	init_tim7_interrupt(100 * hz);
}

void aufgabe10_1_1() {
	init_dac();
	aufgabe_10_aktiv = 1;

	init_sinus_output();
	start_sinus_output(50);
	while (1) {
	}
}

void aufgabe10_1_2() {
	aufgabe_10_aktiv = 2;
	average_window_width = sizeof(average_values) / sizeof(float);
	average_value_index = 0;

	init_dac();
	init_measuring(TYPE_EXTERN_VOLTAGE);
	init_tim7_interrupt(2000);
	while (1) {

	}
}
