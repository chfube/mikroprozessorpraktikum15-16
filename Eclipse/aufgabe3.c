#include "aufgabe3.h"

void init_iwdg(uint8_t prescaler, uint16_t reload) {
	// Schreibrechte aktivieren
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	// den Vorteiler (4, 8 , 16 ,..., 256) setzen
	IWDG_SetPrescaler(prescaler);
	// den Wert (0...4095) einstellen ab dem runtergezählt wird
	IWDG_SetReload(reload);
	// setzt den Wachdog auf den eingestellten Maximalwert
	IWDG_ReloadCounter();
	// aktiviert dem IWDG
	IWDG_Enable();
}

void aufgabe3_1_1() {
	// Taste 1 initialisieren
	GPIO_DeInit(GPIOB);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	init_taste_1();

	// USART 2 initialisieren
	init_usart_2();

	// t = (1/32000) * 64 * 2500 = 5 Sekunden
	init_iwdg(IWDG_Prescaler_64, 2500);

	usart_2_print("\r\nNeustart\r\n");
	while (1) {
		usart_2_print("Schleife\r\n");
		wait_uSek(500000);
		IWDG_ReloadCounter();
		while (read_taste_1()) {
			wait_uSek(500000);
			usart_2_print("Taste1 gedrückt\r\n");
		}
	}

}

void aufgabe3_2_1() {
	unsigned char value_watchdog_counter = 0x7f;
	unsigned char window_value = 0x50;
	unsigned char window_value_refresh = 0x50;
	unsigned char cnt_i = 0;
	unsigned char cnt_j = 0;

	init_usart_2();

	sprintf(usart2_tx_buffer, "\r\nNeustart\r\n");
	usart_2_print(usart2_tx_buffer);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);

	WWDG_SetPrescaler(WWDG_Prescaler_8);
	WWDG_SetWindowValue(window_value);
	WWDG_Enable(value_watchdog_counter);

	cnt_i = (unsigned char) (value_watchdog_counter + 1);

	while (1) {

		cnt_j = (unsigned char) ((WWDG->CR) & 0x7F);

		if (cnt_j < cnt_i) {

			sprintf(usart2_tx_buffer, "i = %u\r\n", cnt_j);
			usart_2_print(usart2_tx_buffer);

			cnt_i = cnt_j;

			if (cnt_i == window_value_refresh) {

				WWDG_SetCounter(value_watchdog_counter);

				sprintf(usart2_tx_buffer, "####### neu geladen\r\n");
				usart_2_print(usart2_tx_buffer);

				cnt_i = (unsigned char) (value_watchdog_counter + 1);
			}
		}
	}
}